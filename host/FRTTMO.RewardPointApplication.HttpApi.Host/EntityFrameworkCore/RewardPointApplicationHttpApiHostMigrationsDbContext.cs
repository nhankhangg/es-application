﻿using Microsoft.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;

namespace FRTTMO.RewardPointApplication.EntityFrameworkCore;

public class RewardPointApplicationHttpApiHostMigrationsDbContext : AbpDbContext<RewardPointApplicationHttpApiHostMigrationsDbContext>
{
    public RewardPointApplicationHttpApiHostMigrationsDbContext(DbContextOptions<RewardPointApplicationHttpApiHostMigrationsDbContext> options)
        : base(options)
    {

    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.ConfigureRewardPointApplication();
    }
}
