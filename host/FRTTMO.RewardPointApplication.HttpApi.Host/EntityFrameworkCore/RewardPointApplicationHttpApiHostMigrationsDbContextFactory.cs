﻿using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace FRTTMO.RewardPointApplication.EntityFrameworkCore;

public class RewardPointApplicationHttpApiHostMigrationsDbContextFactory : IDesignTimeDbContextFactory<RewardPointApplicationHttpApiHostMigrationsDbContext>
{
    public RewardPointApplicationHttpApiHostMigrationsDbContext CreateDbContext(string[] args)
    {
        var configuration = BuildConfiguration();

        var builder = new DbContextOptionsBuilder<RewardPointApplicationHttpApiHostMigrationsDbContext>()
            .UseSqlServer(configuration.GetConnectionString("RewardPointApplication"));

        return new RewardPointApplicationHttpApiHostMigrationsDbContext(builder.Options);
    }

    private static IConfigurationRoot BuildConfiguration()
    {
        var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: false);

        return builder.Build();
    }
}
