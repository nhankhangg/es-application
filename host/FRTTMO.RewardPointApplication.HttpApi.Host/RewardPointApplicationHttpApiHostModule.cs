using Customize.Logging;
using Elastic.Apm;
using Elastic.Apm.DiagnosticSource;
using Elastic.Apm.EntityFrameworkCore;
using Elastic.Apm.NetCoreAll;
using FRTTMO.RewardPointApplication.EntityFrameworkCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using StackExchange.Redis.MultiplexerPool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Volo.Abp;
using Volo.Abp.AspNetCore.Mvc.UI.MultiTenancy;
using Volo.Abp.AspNetCore.Serilog;
using Volo.Abp.Autofac;
using Volo.Abp.Caching.StackExchangeRedis;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore.SqlServer;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;
using Volo.Abp.Swashbuckle;

namespace FRTTMO.RewardPointApplication;

[DependsOn(
    typeof(RewardPointApplicationApplicationModule),
    typeof(RewardPointApplicationEntityFrameworkCoreModule),
    typeof(RewardPointApplicationHttpApiModule),
    typeof(AbpAspNetCoreMvcUiMultiTenancyModule),
    typeof(AbpAutofacModule),

    //▼	==========NOTE: Redis==========
    typeof(AbpCachingStackExchangeRedisModule),
    //▲	==========NOTE: Redis==========

    typeof(AbpEntityFrameworkCoreSqlServerModule),
    //typeof(AbpAuditLoggingEntityFrameworkCoreModule),
    //typeof(AbpPermissionManagementEntityFrameworkCoreModule),
    //typeof(AbpSettingManagementEntityFrameworkCoreModule),
    //typeof(AbpTenantManagementEntityFrameworkCoreModule),
    typeof(AbpAspNetCoreSerilogModule),
    typeof(AbpSwashbuckleModule)
    )]
public class RewardPointApplicationHttpApiHostModule : AbpModule
{
    private readonly string _apiVersion = typeof(RewardPointApplicationHttpApiHostModule).Assembly.GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;
    private readonly string _apiTitle = "Reward Point Application API";

    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        var hostingEnvironment = context.Services.GetHostingEnvironment();
        var configuration = context.Services.GetConfiguration();

        Configure<AbpDbContextOptions>(options =>
        {
            options.UseSqlServer();
        });

        var BuildTime = configuration["BuildTime"] ?? "";
        var BuildTag = configuration["BuildTag"] ?? "";
        context.Services.AddAbpSwaggerGenWithOAuth(
            configuration["AuthServer:Authority"],
            new Dictionary<string, string>
            {
                {"RewardPointApplication", "RewardPointApplication API"}
            },
            options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = _apiTitle,
                    Version = _apiVersion,
                    Description = String.Format("## {0}\n{1}\n### Version: {2}\nTag: __{3}__", hostingEnvironment.EnvironmentName, BuildTime, _apiVersion, BuildTag)
                });
                options.DocInclusionPredicate((docName, description) => true);
                options.CustomSchemaIds(type => type.FullName);
            });

        Configure<AbpLocalizationOptions>(options =>
        {
            options.Languages.Add(new LanguageInfo("ar", "ar", "العربية"));
            options.Languages.Add(new LanguageInfo("cs", "cs", "Čeština"));
            options.Languages.Add(new LanguageInfo("en", "en", "English"));
            options.Languages.Add(new LanguageInfo("en-GB", "en-GB", "English (UK)"));
            options.Languages.Add(new LanguageInfo("fi", "fi", "Finnish"));
            options.Languages.Add(new LanguageInfo("fr", "fr", "Français"));
            options.Languages.Add(new LanguageInfo("hi", "hi", "Hindi", "in"));
            options.Languages.Add(new LanguageInfo("is", "is", "Icelandic", "is"));
            options.Languages.Add(new LanguageInfo("it", "it", "Italiano", "it"));
            options.Languages.Add(new LanguageInfo("hu", "hu", "Magyar"));
            options.Languages.Add(new LanguageInfo("pt-BR", "pt-BR", "Português"));
            options.Languages.Add(new LanguageInfo("ro-RO", "ro-RO", "Română"));
            options.Languages.Add(new LanguageInfo("ru", "ru", "Русский"));
            options.Languages.Add(new LanguageInfo("sk", "sk", "Slovak"));
            options.Languages.Add(new LanguageInfo("tr", "tr", "Türkçe"));
            options.Languages.Add(new LanguageInfo("zh-Hans", "zh-Hans", "简体中文"));
            options.Languages.Add(new LanguageInfo("zh-Hant", "zh-Hant", "繁體中文"));
            options.Languages.Add(new LanguageInfo("de-DE", "de-DE", "Deutsch"));
            options.Languages.Add(new LanguageInfo("es", "es", "Español"));
            options.Languages.Add(new LanguageInfo("el", "el", "Ελληνικά"));
        });

        // Configure<AbpDistributedCacheOptions>(options =>
        // {
        //     options.KeyPrefix = "RewardPointApplication:";
        // });

        // var dataProtectionBuilder = context.Services.AddDataProtection().SetApplicationName("RewardPointApplication");
        // if (!hostingEnvironment.IsDevelopment())
        // {
        //     var redis = ConnectionMultiplexer.Connect(configuration["Redis:Configuration"]);
        //     dataProtectionBuilder.PersistKeysToStackExchangeRedis(redis, "RewardPointApplication-Protection-Keys");
        // }

        //▼	==========NOTE: Redis==========
        var muxer = ConnectionMultiplexerPoolFactory.Create(100, configuration["Redis:Configuration"], null, ConnectionSelectionStrategy.RoundRobin);
        context.Services.AddSingleton<IConnectionMultiplexerPool>(muxer);
        //▲	==========NOTE: Redis==========

        context.Services.AddCors(options =>
        {
            options.AddDefaultPolicy(builder =>
            {
                builder
                    .WithOrigins(
                        configuration["App:CorsOrigins"]
                            .Split(",", StringSplitOptions.RemoveEmptyEntries)
                            .Select(o => o.RemovePostFix("/"))
                            .ToArray()
                    )
                    .WithAbpExposedHeaders()
                    .SetIsOriginAllowedToAllowWildcardSubdomains()
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowCredentials();
            });
        });
    }

    public override void OnApplicationInitialization(ApplicationInitializationContext context)
    {
        var app = context.GetApplicationBuilder();
        var env = context.GetEnvironment();

        //▼	==========NOTE: APM==========
        var _configuration = context.GetConfiguration();
        app.UseAllElasticApm(_configuration);
        //▲	==========NOTE: APM==========

        Agent.Subscribe(new HttpDiagnosticsSubscriber());
        Agent.Subscribe(new EfCoreDiagnosticsSubscriber());

        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }
        else
        {
            app.UseHsts();
        }

        app.UseHttpsRedirection();
        app.UseCorrelationId();
        app.UseStaticFiles();
        app.UseRouting();
        app.UseCors();
        app.UseAuthentication();
        app.UseAbpRequestLocalization();
        app.UseAuthorization();
        app.UseSwagger();
        app.UseMiddleware<RequestResponseLoggerMiddleware>();
        app.UseAbpSwaggerUI(options =>
        {
            options.SwaggerEndpoint("/swagger/v1/swagger.json", "Support APP API");

            var configuration = context.GetConfiguration();
            options.OAuthClientId(configuration["AuthServer:SwaggerClientId"]);
            options.OAuthScopes("RewardPointApplication");
        });
        app.UseAuditing();
        app.UseAbpSerilogEnrichers();
        app.UseConfiguredEndpoints();
    }
}
