﻿using FRTTMO.RewardPointApplication.ExceptionCodes;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Linq;
using Volo.Abp.AspNetCore.Mvc.Validation;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Validation;

namespace FRTTMO.RewardPointApplication.Validations
{
    [Dependency(ReplaceServices = true)]
    [ExposeServices(typeof(IModelStateValidator))]
    public class FrtModelStateValidator : ModelStateValidator
    {
        public override void Validate(ModelStateDictionary modelState)
        {
            var validationResult = new AbpValidationResult();

            base.AddErrors(validationResult, modelState);

            if (validationResult.Errors.Any())
            {
                throw new FrtValidationException(ExceptionCode.BadRequest,
                    "Tham số đầu vào không hợp lệ !",
                    validationResult.Errors);
            }
        }
    }
}