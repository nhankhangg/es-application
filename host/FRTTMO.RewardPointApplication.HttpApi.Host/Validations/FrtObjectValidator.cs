﻿using FRTTMO.RewardPointApplication.ExceptionCodes;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Validation;

namespace FRTTMO.RewardPointApplication.Validations
{
    [Dependency(ReplaceServices = true)]
    [ExposeServices(typeof(IObjectValidator))]
    public class FrtObjectValidator : IObjectValidator, ITransientDependency
    {
        protected IServiceScopeFactory ServiceScopeFactory { get; }
        protected AbpValidationOptions Options { get; }

        public FrtObjectValidator(IOptions<AbpValidationOptions> options, IServiceScopeFactory serviceScopeFactory)
        {
            ServiceScopeFactory = serviceScopeFactory;
            Options = options.Value;
        }

        public virtual async Task ValidateAsync(object validatingObject, string name = null, bool allowNull = false)
        {
            var errors = await GetErrorsAsync(validatingObject, name, allowNull);

            if (errors.Any())
            {
                throw new FrtValidationException(ExceptionCode.BadRequest,
                    "Object state is not valid! See ValidationErrors for details.",
                    errors
                );
            }
        }

        public virtual async Task<List<ValidationResult>> GetErrorsAsync(object validatingObject, string name = null, bool allowNull = false)
        {
            if (validatingObject == null)
            {
                if (allowNull)
                {
                    return new List<ValidationResult>();
                }
                else
                {
                    return new List<ValidationResult>
                    {
                        name == null
                            ? new ValidationResult("Given object is null!")
                            : new ValidationResult(name + " is null!", new[] {name})
                    };
                }
            }

            var context = new ObjectValidationContext(validatingObject);

            using (var scope = ServiceScopeFactory.CreateScope())
            {
                foreach (var contributorType in Options.ObjectValidationContributors)
                {
                    var contributor = (IObjectValidationContributor)
                        scope.ServiceProvider.GetRequiredService(contributorType);
                    await contributor.AddErrorsAsync(context);
                }
            }

            return context.Errors;
        }
    }
}