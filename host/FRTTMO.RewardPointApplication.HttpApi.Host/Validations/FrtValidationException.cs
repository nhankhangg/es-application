﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Volo.Abp.ExceptionHandling;
using Volo.Abp.Validation;

namespace FRTTMO.RewardPointApplication.Validations
{
    public class FrtValidationException : AbpValidationException, IHasErrorCode
    {
        public string Code { get; set; }

        public FrtValidationException(string code, string message, IList<ValidationResult> validationErrors) : base (message, validationErrors)
        {
            Code = code;
        }
    }

}
