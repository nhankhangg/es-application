﻿using System;

namespace FRTTMO.RewardPointApplication.CacheItems
{
    public class ProductItemBarcodeCacheItem
    {
        public string Code { get; set; }
        public string Barcode { get; set; }
        public string ItemCode { get; set; }
        public int TypeCode { get; set; }
        public string TypeName { get; set; }
        public int StatusCode { get; set; }
        public string StatusName { get; set; }
        public string LotNumber { get; set; }
        public DateTime ExpireDate { get; set; }
    }
}
