﻿using System;

namespace FRTTMO.RewardPointApplication.CacheItems
{
    public class RewardItemExcludeBarcodeCacheItem
    {
        public string Sku { get; set; }
        public string TypeCode { get; set; }
        public string TypeName { get; set; }
        public int Exclude_Year { get; set; }
        public int Exclude_Month { get; set; }
        public int LotDate_Year { get; set; }
        public int LotDate_Month { get; set; }
        public DateTime CachingDateTime { get; set; }
        public DateTime CachingExpireDateTime { get; set; }
    }
}
