﻿namespace FRTTMO.RewardPointApplication.CacheItems
{
    public class RewardItemExcludeCacheItem
    {
        public string Sku { get; set; }
        public int LotDate_Year { get; set; }
        public int LotDate_Month { get; set; }
        public string TypeCode { get; set; }
        public string TypeCodeName { get; set; }
        public int Exclude_Month { get; set; }
        public int Exclude_Year { get; set; }
    }
}
