﻿using System;

namespace FRTTMO.RewardPointApplication.CacheItems
{
    public class RewardItemExcludeSkuCacheItem
    {
        public string Sku { get; set; }
        public string TypeCode { get; set; }
        public string TypeName { get; set; }
        public string ShopCode { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime CachingDateTime { get; set; }
        public DateTime CachingExpireDateTime { get; set; }
    }
}
