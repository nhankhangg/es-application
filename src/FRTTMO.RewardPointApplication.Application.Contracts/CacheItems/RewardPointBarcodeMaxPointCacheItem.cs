﻿using System;

namespace FRTTMO.RewardPointApplication.CacheItems
{
    public class RewardPointBarcodeMaxPointCacheItem
    {
        public string Barcode { get; set; }
        public string ShopCode { get; set; }
        public string Sku { get; set; }
        public string UnitCode { get; set; }
        public float Point { get; set; }
        public DateTime ActiveFromDate { get; set; }
        public DateTime ActiveToDate { get; set; }
        public string PointTypeCode { get; set; }
        public Guid RewardPointId { get; set; }
        public string RewardPointCode { get; set; }
        public DateTime CachingDateTime { get; set; }
        public DateTime CachingExpireDateTime { get; set; }
    }
}
