﻿using System;

namespace FRTTMO.RewardPointApplication.CacheItems
{
    public class RewardPointQuotaCache
    {
        public Guid RewardPointId { get; set; }
        public string Sku { get; set; }
        ////////public int Quota { get; set; }
        ////////public int QuotaCurrent { get; set; }
        public int QuotaMinLevel { get; set; }
        public int QuotaCurrentMinLevel { get; set; }
    }
}
