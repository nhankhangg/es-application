﻿using System;

namespace FRTTMO.RewardPointApplication.CacheItems
{
    public class RewardPointSkuCacheItem
    {
        public string Sku { get; set; }
        public string ShopCode { get; set; }
        public float Point { get; set; }
        public DateTime ActiveFromDate { get; set; }
        public DateTime ActiveToDate { get; set; }
        public string UnitCode { get; set; }
        public string UnitName { get; set; }
        public string PointTypeCode { get; set; }
        public Guid RewardPointId { get; set; }
        public string RewardPointCode { get; set; }
        public bool IsQuota { get; set; }
    }
}
