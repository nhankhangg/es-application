﻿using System.Collections.Generic;

namespace FRTTMO.RewardPointApplication.Dtos.PIM
{
    public class PIMListProductDetailDto
    {
        public string Sku { get; set; }
        public string Name { get; set; }
        public List<PIMListProductMeasureDto> Measures { get; set; }
    }

    public class PIMListProductMeasureDto
    {
        public string MeasureUnitName { get; set; }
        public bool IsDefault { get; set; }
        public int Level { get; set; }
        public int UniqueId { get; set; }
        public int Ratio { get; set; }
        public int Price { get; set; }
    }
}
