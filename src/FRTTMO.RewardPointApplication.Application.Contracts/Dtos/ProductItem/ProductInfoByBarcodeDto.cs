﻿using System;

namespace FRTTMO.RewardPointApplication.Dtos.ProductItem
{
    public class ProductInfoByBarcodeDto
    {
        public string ItemCode { get; set; }
        public int StatusCode { get; set; }
        public string StatusName { get; set; }
        public int BarcodeType { get; set; }
        public string BarcodeTypeName { get; set; }
        public string DataSource { get; set; }
        public string LotNumber { get; set; }
        public DateTime ExpireDate { get; set; }
    }
}
