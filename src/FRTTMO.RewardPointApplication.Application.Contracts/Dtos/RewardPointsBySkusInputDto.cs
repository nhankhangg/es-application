﻿using System.ComponentModel.DataAnnotations;

namespace FRTTMO.RewardPointApplication.Dtos
{
    public class RewardPointsBySkusInputDto
    {
        [Required(ErrorMessage = "Sku is Required")]
        public string Sku { get; set; }
        public string Barcode { get; set; }
        [Required(ErrorMessage = "ShopCode is Required")]
        public string ShopCode { get; set; }
        [Required(ErrorMessage = "UnitCode is Required")]
        public string UnitCode { get; set; }
        public double Price { get; set; }
    }
}
