﻿using System;

namespace FRTTMO.RewardPointApplication.Dtos
{
    public class RewardPointsBySkusOutputDto
    {
        public string Sku { get; set; }
        public string Barcode { get; set; }
        public string ShopCode { get; set; }
        public string UnitCode { get; set; }
        public double Price { get; set; }
        public float Point { get; set; }
        public Guid RewardPointId { get; set; }
        public string RewardPointCode { get; set; }
    }
}
