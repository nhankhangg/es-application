﻿using System;

namespace FRTTMO.RewardPointApplication.Dtos
{
    public class RewardPointsDto
    {
        public float Point { get; set; } = 0;
        public string RewardPointCode { get; set; } = String.Empty;
    }
}
