﻿namespace FRTTMO.RewardPointApplication.Dtos
{
    public class RewardPointsInputDto
    {
        public string SearchType { get; set; }
        public string SearchValue { get; set; }
        public string ShopCode { get; set; }
    }
}
