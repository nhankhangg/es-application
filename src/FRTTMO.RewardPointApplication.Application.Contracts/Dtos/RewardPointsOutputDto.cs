﻿using System;

namespace FRTTMO.RewardPointApplication.Dtos
{
    public class RewardPointsOutputDto
    {
        public string Sku { get; set; }
        public string Barcode { get; set; }
        public DateTime? ExpireDate { get; set; }
        public Guid RewardPointId { get; set; }
        public string RewardPointCode { get; set; }
        public string UnitCode { get; set; }
        public string UnitName { get; set; }
        public double Price { get; set; }
    }
}
