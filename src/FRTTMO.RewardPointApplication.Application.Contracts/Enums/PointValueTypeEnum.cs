﻿using System.ComponentModel.DataAnnotations;

namespace FRTTMO.RewardPointApplication.Enums
{
    public enum PointValueTypeEnum
    {
        [Display(Name = "Loại tiền thưởng", Description = "Theo SKU thường")]
        Point,
        [Display(Name = "Loại phần trăm", Description = "Theo SKU cận date / Theo barcode cận date")]
        Percent,
    }
}
