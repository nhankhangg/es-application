﻿using System.ComponentModel.DataAnnotations;

namespace FRTTMO.RewardPointApplication.Enums
{
    public enum RewardItemExcludeBarcodeTypeEnum
    {
        [Display(Name = "Sku", Description = "Loại trừ barcode theo mã")]
        Sku,
        [Display(Name = "ExpireDate", Description = "Loại trừ barcode theo date")]
        ExpireDate,
    }
}
