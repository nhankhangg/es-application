﻿using System.ComponentModel.DataAnnotations;

namespace FRTTMO.RewardPointApplication.Enums
{
    public enum RewardItemExcludeSkuTypeEnum
    {
        [Display(Name = "Shop", Description = "Loại trừ Sku theo Shop")]
        Shop,
    }
}
