﻿using System.ComponentModel.DataAnnotations;

namespace FRTTMO.RewardPointApplication.Enums
{
    public enum RewardItemExcludeTypeEnum
    {
        [Display(Name = "ExcludeBarcodeBySku", Description = "Loại trừ barcode theo mã")]
        ExcludeBarcodeBySku,
        [Display(Name = "ExcludeBarcodeByExpireDate", Description = "Loại trừ barcode theo date")]
        ExcludeBarcodeByExpireDate,
    }
}
