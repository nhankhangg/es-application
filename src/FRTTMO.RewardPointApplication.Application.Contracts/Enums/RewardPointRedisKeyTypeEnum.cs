﻿using System.ComponentModel.DataAnnotations;

namespace FRTTMO.RewardPointApplication.Enums
{
    public enum RewardPointRedisKeyTypeEnum
    {
        [Display(Name = "Sku", Description = "Key Redis Điểm thưởng theo Sku")]
        Sku,
        [Display(Name = "SkuBarcode", Description = "Key Redis Điểm thưởng theo Sku cận date")]
        SkuBarcode,
        [Display(Name = "Barcode", Description = "Key Redis Điểm thưởng theo Barcode")]
        Barcode,
        [Display(Name = "Quota", Description = "Key Redis Quota")]
        Quota,
        [Display(Name = "BarcodeMaxPoint", Description = "Key Redis Điểm thưởng lớn nhất theo Barcode")]
        BarcodeMaxPoint,
        [Display(Name = "ItemExcludeBarcode", Description = "Key Redis Sản phẩm loại trừ Barcode")]
        ItemExcludeBarcode,
        [Display(Name = "ItemExcludeSku", Description = "Key Redis Sản phẩm loại trừ Sku")]
        ItemExcludeSku,
    }
}
