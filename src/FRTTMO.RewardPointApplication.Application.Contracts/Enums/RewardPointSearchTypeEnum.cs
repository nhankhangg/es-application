﻿using System.ComponentModel.DataAnnotations;

namespace FRTTMO.RewardPointApplication.Enums
{
    public enum RewardPointSearchTypeEnum
    {
        [Display(Name = "Sku", Description = "Tìm Điểm thưởng theo Sku")]
        Sku,
        [Display(Name = "Barcode", Description = "Tìm Điểm thưởng theo Barcode")]
        Barcode,
    }
}
