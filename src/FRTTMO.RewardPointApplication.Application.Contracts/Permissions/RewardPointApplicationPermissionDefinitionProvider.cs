﻿using FRTTMO.RewardPointApplication.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace FRTTMO.RewardPointApplication.Permissions;

public class RewardPointApplicationPermissionDefinitionProvider : PermissionDefinitionProvider
{
    public override void Define(IPermissionDefinitionContext context)
    {
        var myGroup = context.AddGroup(RewardPointApplicationPermissions.GroupName, L("Permission:RewardPointApplication"));
    }

    private static LocalizableString L(string name)
    {
        return LocalizableString.Create<RewardPointApplicationResource>(name);
    }
}
