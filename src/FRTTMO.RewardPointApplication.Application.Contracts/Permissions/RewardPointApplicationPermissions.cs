﻿using Volo.Abp.Reflection;

namespace FRTTMO.RewardPointApplication.Permissions;

public class RewardPointApplicationPermissions
{
    public const string GroupName = "RewardPointApplication";

    public static string[] GetAll()
    {
        return ReflectionHelper.GetPublicConstantsRecursively(typeof(RewardPointApplicationPermissions));
    }
}
