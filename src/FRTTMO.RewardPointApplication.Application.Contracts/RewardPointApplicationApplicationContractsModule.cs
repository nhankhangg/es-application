﻿using Volo.Abp.Application;
using Volo.Abp.Modularity;
using Volo.Abp.Authorization;

namespace FRTTMO.RewardPointApplication;

[DependsOn(
    typeof(RewardPointApplicationDomainSharedModule),
    typeof(AbpDddApplicationContractsModule),
    typeof(AbpAuthorizationModule)
    )]
public class RewardPointApplicationApplicationContractsModule : AbpModule
{

}
