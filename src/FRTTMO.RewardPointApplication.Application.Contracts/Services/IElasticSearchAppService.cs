﻿using FRTTMO.RewardPointApplication.Documents;
using FRTTMO.RewardPointApplication.Dtos.ElasticSearch;
using System;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace FRTTMO.RewardPointApplication.Services
{
    public interface IElasticSearchAppService : IApplicationService
    {
        Task<CommonDocumentResultDto<ProductItemBarcodeDocument>> ProductItem_Barcode(string barcode, int skip = 0, int size = 10);
    }
}
