﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace FRTTMO.RewardPointApplication.Services
{
    public interface IHttpApiClientService
    {
        Task<HttpResponseMessage> FetchAsync(string baseUrl, string operation, string payload, string method, string authorization = "", IDictionary<string, string> headers = default);
    }
}
