﻿using FRTTMO.RewardPointApplication.Dtos.PIM;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace FRTTMO.RewardPointApplication.Services
{
    public interface IPIMAppService : IApplicationService
    {
        Task<List<PIMListProductDetailDto>> GetListProductDetailAsync(string sku);
    }
}
