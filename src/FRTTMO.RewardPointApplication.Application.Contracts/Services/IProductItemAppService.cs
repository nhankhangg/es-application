﻿using FRTTMO.RewardPointApplication.Dtos.ProductItem;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace FRTTMO.RewardPointApplication.Services
{
    public interface IProductItemAppService : IApplicationService
    {
        Task<ProductInfoByBarcodeDto> GetProductInfoByBarcode(string barcode);
    }
}
