﻿using FRTTMO.RewardPointApplication.CacheItems;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace FRTTMO.RewardPointApplication.Services
{
    public interface IRedisCachingAppService : IApplicationService
    {
        Task<RewardPointSkuCacheItem> GetRewardPointSkuWithHash(string skuCode, string shopCode, string unitCode);
        Task<List<RewardPointSkuCacheItem>> GetListRewardPointSkuWithHash(string skuCode, string shopCode, List<string> unitCodes);
        Task<RewardPointSkuBarcodeCacheItem> GetRewardPointSkuBarcode(string barcode, string shopCode);
        Task<RewardPointBarcodeCacheItem> GetRewardPointBarcode(string shopCode);

        #region ===BarcodeMaxPoint===
        Task SetRewardPointBarcodeMaxPoint(string barcode, string shopCode, RewardPointBarcodeMaxPointCacheItem item, TimeSpan expiryTimeSpan);
        Task SetRewardPointBarcodeMaxPointWithHash(string barcode, string shopCode, string unitCode, RewardPointBarcodeMaxPointCacheItem item, TimeSpan expiryTimeSpan);
        Task<RewardPointBarcodeMaxPointCacheItem> GetRewardPointBarcodeMaxPoint(string barcode, string shopCode);
        Task<RewardPointBarcodeMaxPointCacheItem> GetRewardPointBarcodeMaxPointWithHash(string barcode, string shopCode, string unitCode);
        Task RemoveAllRewardPointBarcodeMaxPoint();
        #endregion ===BarcodeMaxPoint===

        Task<RewardPointQuotaCache> GetRewardQuota(Guid rewardPointId, string skuCode);

        #region ===RewardItemExcludeBarcode===
        Task<RewardItemExcludeBarcodeCacheItem> GetRewardItemExcludeBarcode(string sku);
        #endregion ===RewardItemExcludeBarcode===

        #region ===RewardItemExcludeSku===
        //Task<RewardItemExcludeSkuCacheItem> GetRewardItemExcludeSku(string sku);
        #endregion ===RewardItemExcludeSku===

        Task<ProductItemBarcodeCacheItem> GetProductItemBarcode(string barcode);

        #region ===API===
        Task<bool> RemoveAllRewardPointBarcodeMaxPointCache();
        #endregion ===API===

    }
}
