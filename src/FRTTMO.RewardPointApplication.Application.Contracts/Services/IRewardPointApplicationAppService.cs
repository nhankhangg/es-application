﻿using FRTTMO.RewardPointApplication.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace FRTTMO.RewardPointApplication.Services
{
    public interface IRewardPointApplicationAppService : IApplicationService
    {
        string GetCode();

        Task<List<RewardPointsOutputDto>> GetRewardPoints(RewardPointsInputDto input);
        //  [RewardPointApplication] API get thông tin điểm thưởng theo SKU
        //  https://reqs.fptshop.com.vn/browse/LCIMS-355
        Task<List<RewardPointsBySkusOutputDto>> GetRewardPointsBySkus(List<RewardPointsBySkusInputDto> input);
    }
}
