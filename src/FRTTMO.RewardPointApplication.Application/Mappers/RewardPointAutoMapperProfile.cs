﻿using AutoMapper;
using FRTTMO.RewardPointApplication.Dtos;
using System;

namespace FRTTMO.RewardPointApplication.Mappers
{
    public class RewardPointAutoMapperProfile : Profile
    {
        public RewardPointAutoMapperProfile()
        {
            CreateMap<RewardPointsBySkusInputDto, RewardPointsBySkusOutputDto>()
                .ForMember(dest => dest.Point, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.RewardPointId, opt => opt.MapFrom(src => Guid.Empty))
                .ForMember(dest => dest.RewardPointCode, opt => opt.MapFrom(src => string.Empty));
        }
    }
}
