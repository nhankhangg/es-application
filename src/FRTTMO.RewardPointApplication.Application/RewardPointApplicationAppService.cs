﻿using FRTTMO.RewardPointApplication.Localization;
using Volo.Abp.Application.Services;

namespace FRTTMO.RewardPointApplication;

public abstract class RewardPointApplicationAppService : ApplicationService
{
    protected RewardPointApplicationAppService()
    {
        LocalizationResource = typeof(RewardPointApplicationResource);
        ObjectMapperContext = typeof(RewardPointApplicationApplicationModule);
    }
}
