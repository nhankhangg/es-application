﻿using AutoMapper;

namespace FRTTMO.RewardPointApplication;

public class RewardPointApplicationApplicationAutoMapperProfile : Profile
{
    public RewardPointApplicationApplicationAutoMapperProfile()
    {
        /* You can configure your AutoMapper mapping configuration here.
         * Alternatively, you can split your mapping configurations
         * into multiple profile classes for a better organization. */
    }
}
