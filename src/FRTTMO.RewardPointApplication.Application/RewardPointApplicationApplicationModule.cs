﻿using FRTTMO.RewardPointApplication.ElasticSearch;
using Microsoft.Extensions.DependencyInjection;
using StackExchange.Redis;
using Volo.Abp.Application;
using Volo.Abp.AutoMapper;
using Volo.Abp.Modularity;

namespace FRTTMO.RewardPointApplication;

[DependsOn(
    typeof(RewardPointApplicationDomainModule),
    typeof(RewardPointApplicationApplicationContractsModule),

    //▼	==========NOTE: ElasticSearch==========
    typeof(RewardPointApplicationElasticSearchModule),
    //▲	==========NOTE: ElasticSearch==========

    typeof(AbpDddApplicationModule),
    typeof(AbpAutoMapperModule)
    )]
public class RewardPointApplicationApplicationModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        context.Services.AddAutoMapperObjectMapper<RewardPointApplicationApplicationModule>();
        Configure<AbpAutoMapperOptions>(options =>
        {
            options.AddMaps<RewardPointApplicationApplicationModule>(validate: true);
        });

        //▼	==========NOTE: Redis==========
        IConnectionMultiplexer multiplexer = ConnectionMultiplexer.Connect(context.Services.GetConfiguration()["Redis:Configuration"]);
        context.Services.AddSingleton<IConnectionMultiplexer>(multiplexer);
        context.Services.AddScoped<IDatabase>(cfg => multiplexer.GetDatabase());
        //▲	==========NOTE: Redis==========

        //▼	==========NOTE: HttpClient==========
        context.Services.AddHttpClient();
        //▲	==========NOTE: HttpClient==========

    }
}
