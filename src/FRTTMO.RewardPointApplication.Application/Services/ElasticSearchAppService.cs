﻿using FRTTMO.RewardPointApplication.Documents;
using FRTTMO.RewardPointApplication.Dtos.ElasticSearch;
using FRTTMO.RewardPointApplication.ElasticSearch;
using FRTTMO.RewardPointApplication.ExceptionCodes;
using FRTTMO.RewardPointApplication.Localization;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using Nest;
using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace FRTTMO.RewardPointApplication.Services
{
    public class ElasticSearchAppService : ApplicationService, IElasticSearchAppService
    {
        private readonly IStringLocalizer<RewardPointApplicationResource> _stringLocalizer;
        private readonly IOptions<ElasticsearchOptions> _elasticsearchOptions;
        private readonly IElasticsearchService _elasticsearchService;

        private string _indexProductItemBarcode;

        public ElasticSearchAppService(IStringLocalizer<RewardPointApplicationResource> stringLocalizer, IOptions<ElasticsearchOptions> elasticsearchOptions,
            IElasticsearchService elasticsearchService
            )
        {
            _stringLocalizer = stringLocalizer;
            _elasticsearchOptions = elasticsearchOptions;
            _elasticsearchService = elasticsearchService;

            _indexProductItemBarcode = _elasticsearchOptions.Value.IndexProductItemBarcode;
        }

        /// <summary>Get ProductItemBarcode BeforeDate Active by Sku</summary>
        public async Task<CommonDocumentResultDto<ProductItemBarcodeDocument>> ProductItem_Barcode(string barcode, int skip = 0, int size = 10)
        {
            try
            {
                var result = new CommonDocumentResultDto<ProductItemBarcodeDocument>();

                if (string.IsNullOrEmpty(barcode))
                {
                    return result;
                }

                SearchRequest request = new SearchRequest(_indexProductItemBarcode)
                {
                    Query = new TermQuery()
                    {
                        Field = "barcode",
                        Value = barcode
                    }
                };
                var response = await _elasticsearchService.SearchAsync<ProductItemBarcodeDocument, string>(request, skip, size);

                if (response.Documents.Count > 0)
                {
                    result.Count = response.Documents.Count;
                    result.Documents = response.Documents.Select(p => p).ToList();
                }

                return result;
            }
            catch (FrtValidationException ex)
            {
                throw ex.GetBaseException();
            }
            catch (Exception ex)
            {
                throw new FrtValidationException(ErrorCodes.Error, _stringLocalizer[ErrorCodes.Error, $"{MethodBase.GetCurrentMethod().ReflectedType.FullName}: ex.Message: {ex.Message}!"]).WithData("Key", ex.Message);
            }
        }
    }
}
