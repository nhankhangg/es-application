﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace FRTTMO.RewardPointApplication.Services
{
    public class HttpApiClientService : ApplicationService, IHttpApiClientService
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public HttpApiClientService(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public async Task<HttpResponseMessage> FetchAsync(string baseUrl, string operation, string payload, string method, string authorization = "", IDictionary<string, string> headers = default)
        {
            HttpResponseMessage response = null;
            using (var client = _httpClientFactory.CreateClient())
            using (var httpContent = new StringContent(payload, Encoding.UTF8, "application/json"))
            {
                if (!string.IsNullOrEmpty(authorization))
                {
                    client.DefaultRequestHeaders.Add("Authorization", authorization);
                }
                if (headers != default)
                {
                    foreach (KeyValuePair<string, string> header in headers)
                    {
                        client.DefaultRequestHeaders.Add(header.Key, header.Value);
                    }
                }
                client.BaseAddress = new Uri(baseUrl.EnsureEndsWith('/'));
                client.Timeout = TimeSpan.FromMinutes(5);
                switch (method.ToUpper())
                {
                    case "GET":
                        response = await client.GetAsync(operation.RemovePreFix("/"));
                        break;
                    case "POST":
                        response = await client.PostAsync(operation.RemovePreFix("/"), httpContent);
                        break;
                    case "PUT":
                        response = await client.PutAsync(operation.RemovePreFix("/"), httpContent);
                        break;
                    default:
                        break;
                };
            }
            if (response is { IsSuccessStatusCode: true })
            {
                response.EnsureSuccessStatusCode();
                return response;
            }
            else
            {
                return response;
            }
        }
    }
}
