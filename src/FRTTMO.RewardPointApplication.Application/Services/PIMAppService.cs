﻿using FRTTMO.RewardPointApplication.Dtos.PIM;
using FRTTMO.RewardPointApplication.Localization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace FRTTMO.RewardPointApplication.Services
{
    public class PIMAppService : ApplicationService, IPIMAppService
    {
        private readonly IStringLocalizer<RewardPointApplicationResource> _stringLocalizer;
        private readonly IConfiguration _configuration;
        private readonly IHttpApiClientService _httpApiClientService;

        private string _baseUrl = string.Empty;

        public PIMAppService(IStringLocalizer<RewardPointApplicationResource> stringLocalizer, IConfiguration configuration,
            IHttpApiClientService httpApiClientService)
        {
            _stringLocalizer = stringLocalizer;
            _configuration = configuration;
            _httpApiClientService = httpApiClientService;

            _baseUrl = _configuration["RemoteServices:PIMAPI:BaseUrl"].ToString().EnsureEndsWith('/');
        }

        public async Task<List<PIMListProductDetailDto>> GetListProductDetailAsync(string sku)
        {
            var payload = new
            {
                skus = new List<string>()
                {
                    sku
                }
            };
            var response = await _httpApiClientService.FetchAsync(
                _baseUrl,
                "api/products/reward-point/product/details",
                JsonConvert.SerializeObject(payload),
                "POST"
                );
            if (!response.IsSuccessStatusCode)
            {
                return new List<PIMListProductDetailDto>();
            }
            
            var content = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<PIMListProductDetailDto>>(content);
        }
    }
}
