﻿using FRTTMO.RewardPointApplication.Dtos.ProductItem;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace FRTTMO.RewardPointApplication.Services
{
    public class ProductItemAppService : ApplicationService, IProductItemAppService
    {
        private readonly IConfiguration _configuration;
        private readonly IHttpApiClientService _httpApiClientService;

        private string _baseUrl = string.Empty;

        public ProductItemAppService(IConfiguration configuration, IHttpApiClientService httpApiClientService)
        {
            _configuration = configuration;
            _httpApiClientService = httpApiClientService;

            _baseUrl = _configuration["RemoteServices:ProductItemGenerationAPI:BaseUrl"].ToString().EnsureEndsWith('/');
        }

        /// <summary>Lấy Thông tin Sản phẩm theo Barcode</summary>
        public async Task<ProductInfoByBarcodeDto> GetProductInfoByBarcode(string barcode)
        {
            var payload = new
            {
                barcode = barcode
            };
            var response = await _httpApiClientService.FetchAsync(
                _baseUrl,
                "api/Barcode/GetProductInfoByBarcode",
                JsonConvert.SerializeObject(payload),
                "POST"
                );
            if (!response.IsSuccessStatusCode)
            {
                return new ProductInfoByBarcodeDto();
            }

            var content = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<ProductInfoByBarcodeDto>(content);
        }
    }
}
