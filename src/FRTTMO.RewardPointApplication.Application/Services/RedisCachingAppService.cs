﻿using FRTTMO.RewardPointApplication.CacheItems;
using FRTTMO.RewardPointApplication.Enums;
using FRTTMO.RewardPointApplication.ExceptionCodes;
using FRTTMO.RewardPointApplication.Localization;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using StackExchange.Redis;
using StackExchange.Redis.MultiplexerPool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace FRTTMO.RewardPointApplication.Services
{
    public class RedisCachingAppService : ApplicationService, IRedisCachingAppService
    {
        private readonly IConfiguration _configuration;
        private readonly IConnectionMultiplexerPool _connectionMultiplexerPool;
        private readonly IDatabase _databaseRedisCache;
        private readonly IStringLocalizer<RewardPointApplicationResource> _stringLocalizer;

        private readonly string _rootKeyRewardPoints = string.Empty;
        private readonly string _rootKeyProductItem = string.Empty;

        public RedisCachingAppService(IConfiguration configuration, IConnectionMultiplexerPool connectionMultiplexerPool,
            IDatabase databaseRedisCache, IStringLocalizer<RewardPointApplicationResource> stringLocalizer
            )
        {
            _configuration = configuration;
            _connectionMultiplexerPool = connectionMultiplexerPool;
            _databaseRedisCache = databaseRedisCache;
            _stringLocalizer = stringLocalizer;

            _rootKeyRewardPoints = $"{_configuration["Redis:KeyPrefix:Reward"]}";
            _rootKeyProductItem = $"{_configuration["Redis:KeyPrefix:ProductItem"]}";
        }

        #region ===Extension Methods===
        /// <summary>GetDistributedCacheEntryOptions</summary>
        private DistributedCacheEntryOptions GetDistributedCacheEntryOptions(TimeSpan? timeSpan = null)
        {
            timeSpan ??= TimeSpan.FromDays(1).Add(TimeSpan.FromSeconds(-1));
            return new DistributedCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = timeSpan
            };
        }

        /// <summary>GetDatabase</summary>
        private async Task<IDatabaseAsync> GetDatabase()
        {
            var connection = await _connectionMultiplexerPool.GetAsync();
            try
            {
                return connection.Connection.GetDatabase();
            }
            catch (RedisConnectionException)
            {
                await connection.ReconnectAsync();
                return connection.Connection.GetDatabase();
            }
        }

        /// <summary>DeleteWithPatternKey</summary>
        private async Task DeleteWithPatternKey(string PatternKey)
        {
            var patternKey = $"{PatternKey}*";
            var db = await GetDatabase();
            EndPoint endPoint = db.Multiplexer.GetEndPoints().FirstOrDefault();
            RedisKey[] keys = db.Multiplexer.GetServer(endPoint).Keys(pattern: patternKey).ToArray();
            await db.KeyDeleteAsync(keys);
        }
        #endregion ===Extension Methods===

        /// <summary>Lấy Redis key của RewardPoint</summary>
        private string GetRewardPointRedisKey(RewardPointRedisKeyTypeEnum rewardPointType, string Suffix)
        {
            return $"{_rootKeyRewardPoints}:{rewardPointType}:{Suffix}";
        }
        /// <summary>Lấy Redis key của ProductItem</summary>
        private string GetProductItemRedisKey(string Suffix)
        {
            return $"{_rootKeyProductItem}:Barcodes:Barcode:{Suffix}";
        }

        /// <summary>Lấy RewardPointSku từ Redis với Hash</summary>
        public async Task<RewardPointSkuCacheItem> GetRewardPointSkuWithHash(string skuCode, string shopCode, string unitCode)
        {
            var suffix = $"{skuCode}:{shopCode}";
            var key = GetRewardPointRedisKey(RewardPointRedisKeyTypeEnum.Sku, suffix);
            var value = await _databaseRedisCache.HashGetAsync(key, unitCode);
            if (value.HasValue)
            {
                return JsonConvert.DeserializeObject<RewardPointSkuCacheItem>(value);
            }
            return null;
        }

        /// <summary>Lấy list RewardPointSku từ Redis với Hash</summary>
        public async Task<List<RewardPointSkuCacheItem>> GetListRewardPointSkuWithHash(string skuCode, string shopCode, List<string> unitCodes)
        {
            var result = new List<RewardPointSkuCacheItem>();

            var suffix = $"{skuCode}:{shopCode}";
            var key = GetRewardPointRedisKey(RewardPointRedisKeyTypeEnum.Sku, suffix);
            var hashFields = unitCodes.Select(p => (RedisValue)p).ToArray();
            var values = await _databaseRedisCache.HashGetAsync(key, hashFields);
            if (values.Count() > 0)
            {
                result = values.Select(p => JsonConvert.DeserializeObject<RewardPointSkuCacheItem>(p)).ToList();
            }

            return result;
        }

        /// <summary>Lấy RewardPointSkuBarcode từ Redis</summary>
        public async Task<RewardPointSkuBarcodeCacheItem> GetRewardPointSkuBarcode(string barcode, string shopCode)
        {
            var suffix = $"{barcode}:{shopCode}";
            var key = GetRewardPointRedisKey(RewardPointRedisKeyTypeEnum.SkuBarcode, suffix);
            var value = await _databaseRedisCache.StringGetAsync(key);
            if (value.HasValue)
            {
                return JsonConvert.DeserializeObject<RewardPointSkuBarcodeCacheItem>(value);
            }
            return null;
        }

        /// <summary>Lấy RewardPointBarcode từ Redis</summary>
        public async Task<RewardPointBarcodeCacheItem> GetRewardPointBarcode(string shopCode)
        {
            var suffix = $"{shopCode}";
            var key = GetRewardPointRedisKey(RewardPointRedisKeyTypeEnum.Barcode, suffix);
            var value = await _databaseRedisCache.StringGetAsync(key);
            if (value.HasValue)
            {
                return JsonConvert.DeserializeObject<RewardPointBarcodeCacheItem>(value);
            }
            return null;
        }

        #region ===BarcodeMaxPoint===
        /// <summary>Đẩy RewardPointBarcodeMaxPoint lên Redis</summary>
        public async Task SetRewardPointBarcodeMaxPoint(string barcode, string shopCode, RewardPointBarcodeMaxPointCacheItem item, TimeSpan expiryTimeSpan)
        {
            var suffix = $"{barcode}:{shopCode}";
            var key = GetRewardPointRedisKey(RewardPointRedisKeyTypeEnum.BarcodeMaxPoint, suffix);
            await _databaseRedisCache.StringSetAsync(key, JsonConvert.SerializeObject(item), expiryTimeSpan);
        }
        /// <summary>Đẩy RewardPointBarcodeMaxPoint lên Redis với Hash</summary>
        public async Task SetRewardPointBarcodeMaxPointWithHash(string barcode, string shopCode, string unitCode, RewardPointBarcodeMaxPointCacheItem item, TimeSpan expiryTimeSpan)
        {
            var suffix = $"{barcode}:{shopCode}";
            var key = GetRewardPointRedisKey(RewardPointRedisKeyTypeEnum.BarcodeMaxPoint, suffix);
            var listHashEntry = new List<HashEntry>();
            listHashEntry.Add(new HashEntry(unitCode, JsonConvert.SerializeObject(item)));
            await _databaseRedisCache.HashSetAsync(key, listHashEntry.ToArray());
            await _databaseRedisCache.KeyExpireAsync(key, expiryTimeSpan);
        }

        /// <summary>Lấy RewardPointBarcodeMaxPoint từ Redis</summary>
        public async Task<RewardPointBarcodeMaxPointCacheItem> GetRewardPointBarcodeMaxPoint(string barcode, string shopCode)
        {
            var suffix = $"{barcode}:{shopCode}";
            var key = GetRewardPointRedisKey(RewardPointRedisKeyTypeEnum.BarcodeMaxPoint, suffix);
            var value = await _databaseRedisCache.StringGetAsync(key);
            if (value.HasValue)
            {
                return JsonConvert.DeserializeObject<RewardPointBarcodeMaxPointCacheItem>(value);
            }
            return null;
        }

        /// <summary>Lấy RewardPointBarcodeMaxPoint từ Redis với Hash</summary>
        public async Task<RewardPointBarcodeMaxPointCacheItem> GetRewardPointBarcodeMaxPointWithHash(string barcode, string shopCode, string unitCode)
        {
            var suffix = $"{barcode}:{shopCode}";
            var key = GetRewardPointRedisKey(RewardPointRedisKeyTypeEnum.BarcodeMaxPoint, suffix);
            var value = await _databaseRedisCache.HashGetAsync(key, unitCode);
            if (value.HasValue)
            {
                return JsonConvert.DeserializeObject<RewardPointBarcodeMaxPointCacheItem>(value);
            }
            return null;
        }

        /// <summary>Xóa All RewardPointBarcodeMaxPoint từ Redis</summary>
        public async Task RemoveAllRewardPointBarcodeMaxPoint()
        {
            var patternKey = $"{_rootKeyRewardPoints}:{RewardPointRedisKeyTypeEnum.BarcodeMaxPoint}:";
            await DeleteWithPatternKey(patternKey);
        }
        #endregion ===BarcodeMaxPoint===

        /// <summary>Lấy Reward Quota từ Redis</summary>
        public async Task<RewardPointQuotaCache> GetRewardQuota(Guid rewardPointId, string skuCode)
        {
            var suffix = $"{rewardPointId}:{skuCode}";
            var key = GetRewardPointRedisKey(RewardPointRedisKeyTypeEnum.Quota, suffix);
            var value = await _databaseRedisCache.StringGetAsync(key);
            if (value.HasValue)
            {
                return JsonConvert.DeserializeObject<RewardPointQuotaCache>(value);
            }
            return null;
        }

        #region ===RewardItemExcludeBarcode===
        /// <summary>Lấy RewardItemExcludeBarcode từ Redis</summary>
        public async Task<RewardItemExcludeBarcodeCacheItem> GetRewardItemExcludeBarcode(string sku)
        {
            var suffix = $"{sku}";
            var key = GetRewardPointRedisKey(RewardPointRedisKeyTypeEnum.ItemExcludeBarcode, suffix);
            var value = await _databaseRedisCache.StringGetAsync(key);
            if (value.HasValue)
            {
                return JsonConvert.DeserializeObject<RewardItemExcludeBarcodeCacheItem>(value);
            }
            return null;
        }
        #endregion ===RewardItemExcludeBarcode===

        #region ===RewardItemExcludeSku===
        ///// <summary>Lấy RewardItemExcludeSku từ Redis</summary>
        //public async Task<RewardItemExcludeSkuCacheItem> GetRewardItemExcludeSku(string sku)
        //{
        //    var suffix = $"{sku}";
        //    var key = GetRewardPointRedisKey(RewardPointRedisKeyTypeEnum.ItemExcludeSku, suffix);
        //    var value = await _databaseRedisCache.StringGetAsync(key);
        //    if (value.HasValue)
        //    {
        //        return JsonConvert.DeserializeObject<RewardItemExcludeSkuCacheItem>(value);
        //    }
        //    return null;
        //}
        #endregion ===RewardItemExcludeSku===

        /// <summary>Lấy ProductItemBarcode từ Redis</summary>
        public async Task<ProductItemBarcodeCacheItem> GetProductItemBarcode(string barcode)
        {
            var suffix = $"{barcode}";
            var key = GetProductItemRedisKey(suffix);
            var value = await _databaseRedisCache.HashGetAsync(key, "data");
            if (value.HasValue)
            {
                return JsonConvert.DeserializeObject<ProductItemBarcodeCacheItem>(value);
            }
            return null;
        }

        #region ===API===
        /// <summary>Xóa All RewardPointBarcodeMaxPoint từ Redis</summary>
        public async Task<bool> RemoveAllRewardPointBarcodeMaxPointCache()
        {
            try
            {
                await RemoveAllRewardPointBarcodeMaxPoint();
                return true;
            }
            catch (FrtValidationException ex)
            {
                throw ex.GetBaseException();
            }
            catch (Exception ex)
            {
                // mã lỗi 
                throw new FrtValidationException(ErrorCodes.Error, _stringLocalizer[ErrorCodes.Error, $"{MethodBase.GetCurrentMethod().ReflectedType.FullName}: ex.Message: {ex.Message}!"]).WithData("Key", ex.Message);
            }
        }
        #endregion ===API===

    }
}
