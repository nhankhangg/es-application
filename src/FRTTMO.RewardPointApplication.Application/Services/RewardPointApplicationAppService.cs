﻿using Elastic.Apm;
using Elastic.Apm.Api;
using FRTTMO.RewardPointApplication.CacheItems;
using FRTTMO.RewardPointApplication.Dtos;
using FRTTMO.RewardPointApplication.Dtos.PIM;
using FRTTMO.RewardPointApplication.Enums;
using FRTTMO.RewardPointApplication.ExceptionCodes;
using FRTTMO.RewardPointApplication.Localization;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace FRTTMO.RewardPointApplication.Services
{
    public class RewardPointApplicationAppService : ApplicationService, IRewardPointApplicationAppService
    {
        private readonly ITracer _tracer;
        private readonly IStringLocalizer<RewardPointApplicationResource> _stringLocalizer;
        private readonly IRedisCachingAppService _redisCachingAppService;
        private readonly IPIMAppService _pIMAppService;
        private readonly IProductItemAppService _productItemAppService;

        public RewardPointApplicationAppService(IStringLocalizer<RewardPointApplicationResource> stringLocalizer, IRedisCachingAppService redisCachingAppService,
            IPIMAppService pIMAppService, IProductItemAppService productItemAppService
            )
        {
            _tracer = Agent.Tracer;
            _stringLocalizer = stringLocalizer;
            _redisCachingAppService = redisCachingAppService;
            _pIMAppService = pIMAppService;
            _productItemAppService = productItemAppService;
        }


        /// <summary>Lấy code</summary>
        public string GetCode()
        {
            return "202311180133";
        }

        /// <summary>Get RewardPoints</summary>
        public async Task<List<RewardPointsOutputDto>> GetRewardPoints(RewardPointsInputDto input)
        {
            var span = _tracer.CurrentTransaction?.StartSpan($"GetRewardPoints( - SearchType: {input.SearchType} - SearchValue: {input.SearchValue} - ShopCode: {input.ShopCode} )", "app");
            try
            {
                var result = new List<RewardPointsOutputDto>();

                var result_Sku = string.Empty;
                var result_Barcode = string.Empty;
                DateTime? result_ExpireDate = null;
                var result_RewardPointId = Guid.Empty;
                var result_RewardPointCode = string.Empty;
                var result_UnitCode = string.Empty;
                var result_UnitName = string.Empty;
                double result_Price = 0;

                var search_Sku = string.Empty;
                var search_Barcode = string.Empty;
                DateTime? search_ExpireDate = null;
                var search_ShopCode = string.Empty;
                var search_UnitCode = string.Empty;
                var search_UnitName = string.Empty;
                double search_Price = 0;

                if (input.SearchType == $"{RewardPointSearchTypeEnum.Sku}")
                {
                    search_Sku = input.SearchValue;
                    search_Barcode = "";
                    search_ShopCode = input.ShopCode;
                    search_UnitCode = "";
                    search_UnitName = "";
                    search_Price = 0;
                }
                else if (input.SearchType == $"{RewardPointSearchTypeEnum.Barcode}")
                {
                    //  ===Gọi sang ProductItem===
                    try
                    {
                        var productItemBarcode = await _productItemAppService.GetProductInfoByBarcode(input.SearchValue);
                        if (productItemBarcode != null)
                        {
                            search_Sku = productItemBarcode.ItemCode;
                            search_Barcode = input.SearchValue;
                            search_ExpireDate = productItemBarcode.ExpireDate.Date;
                            search_ShopCode = input.ShopCode;
                            search_UnitCode = "";
                            search_UnitName = "";
                            search_Price = 0;
                        }
                    }
                    catch (Exception)
                    {
                        //  Do nothing
                    }
                }

                if (!string.IsNullOrEmpty(search_Sku))
                {
                    #region ===Get Product Detail from PIM===
                    //  ===Get Product Detail from PIM===
                    var listProductDetail = new List<PIMListProductDetailDto>();
                    try
                    {
                        listProductDetail = await _pIMAppService.GetListProductDetailAsync(search_Sku);
                    }
                    catch (Exception)
                    {
                        //  Do nothing
                    }
                    if (listProductDetail != null && listProductDetail.Count > 0)
                    {
                        var productDetail = listProductDetail.Where(p => p.Sku == search_Sku).FirstOrDefault();
                        if (productDetail != null)
                        {
                            var listMeasures = productDetail.Measures;
                            if (listMeasures != null && listMeasures.Count > 0)
                            {
                                var measure = listMeasures.Where(p => p.IsDefault) // Lấy Đơn vị bán
                                    .OrderBy(p => p.Level) // Đơn vị cao nhất (Level nhỏ nhất)
                                    .FirstOrDefault();
                                if (measure != null)
                                {
                                    search_UnitCode = measure.UniqueId.ToString();
                                    search_UnitName = measure.MeasureUnitName;
                                    search_Price = measure.Price;
                                }
                            }
                        }
                    }
                    #endregion ===Get Product Detail from PIM===

                    #region ===Call API GetRewardPointsBySkus===
                    var listRewardPointsBySkusInputDto = new List<RewardPointsBySkusInputDto>();
                    var rewardPointsBySkusInputDto = new RewardPointsBySkusInputDto()
                    {
                        Sku = search_Sku,
                        Barcode = search_Barcode,
                        ShopCode = search_ShopCode,
                        UnitCode = search_UnitCode,
                        Price = search_Price
                    };
                    listRewardPointsBySkusInputDto.Add(rewardPointsBySkusInputDto);
                    var listRewardPointsBySkus = new List<RewardPointsBySkusOutputDto>();
                    try
                    {
                        listRewardPointsBySkus = await GetRewardPointsBySkus(listRewardPointsBySkusInputDto);
                    }
                    catch (Exception)
                    {
                        //  Do nothing
                    }
                    if (listRewardPointsBySkus != null && listRewardPointsBySkus.Count > 0)
                    {
                        var rewardPointsBySkus = listRewardPointsBySkus.FirstOrDefault();
                        if (rewardPointsBySkus != null)
                        {
                            result_Sku = rewardPointsBySkus.Sku;
                            result_Barcode = rewardPointsBySkus.Barcode;
                            result_ExpireDate = search_ExpireDate != null ? search_ExpireDate.Value.Date : null;
                            result_RewardPointId = rewardPointsBySkus.RewardPointId;
                            result_RewardPointCode = rewardPointsBySkus.RewardPointCode;
                            result_UnitCode = rewardPointsBySkus.UnitCode;
                            result_UnitName = search_UnitName;
                            result_Price = rewardPointsBySkus.Price;

                            if (result_RewardPointId != Guid.Empty)
                            {
                                RewardPointsOutputDto rewardPointsOutput = new RewardPointsOutputDto()
                                {
                                    Sku = result_Sku,
                                    Barcode = result_Barcode,
                                    ExpireDate = result_ExpireDate != null ? result_ExpireDate.Value.Date : null,
                                    RewardPointId = result_RewardPointId,
                                    RewardPointCode = result_RewardPointCode,
                                    UnitCode = result_UnitCode,
                                    UnitName = result_UnitName,
                                    Price = result_Price
                                };
                                result.Add(rewardPointsOutput);
                            }
                        }
                    }
                    #endregion ===Call API GetRewardPointsBySkus===
                }

                return result;
            }
            catch (FrtValidationException ex)
            {
                throw ex.GetBaseException();
            }
            catch (Exception ex)
            {
                throw new FrtValidationException(ErrorCodes.Error, _stringLocalizer[ErrorCodes.Error, $"{MethodBase.GetCurrentMethod().ReflectedType.FullName}: ex.Message: {ex.Message}!"]).WithData("Key", ex.Message);
            }
            finally
            {
                span?.End();
            }
        }

        /// <summary>Get RewardPoints By Skus</summary>
        public async Task<List<RewardPointsBySkusOutputDto>> GetRewardPointsBySkus(List<RewardPointsBySkusInputDto> input)
        {
            var span = _tracer.CurrentTransaction?.StartSpan($"GetRewardPointsBySkus()", "app");
            try
            {
                var result = new List<RewardPointsBySkusOutputDto>();

                foreach (var skuItem in input)
                {
                    var skuItemOutput = ObjectMapper.Map<RewardPointsBySkusInputDto, RewardPointsBySkusOutputDto>(skuItem);

                    #region ===Get RewardPointSku===
                    var rewardPointSku = await _redisCachingAppService.GetRewardPointSkuWithHash(skuItem.Sku, skuItem.ShopCode, skuItem.UnitCode);
                    if (rewardPointSku != null)
                    {
                        skuItemOutput.Point = 0;
                        skuItemOutput.RewardPointId = rewardPointSku.RewardPointId;
                        skuItemOutput.RewardPointCode = rewardPointSku.RewardPointCode;

                        if (rewardPointSku.Point > 0)
                        {
                            if (rewardPointSku.IsQuota)
                            {
                                var quota = await _redisCachingAppService.GetRewardQuota(rewardPointSku.RewardPointId, rewardPointSku.Sku);
                                if (quota?.QuotaCurrentMinLevel > 0)
                                {
                                    //  ===Nếu còn Quota thì trả điểm thưởng về===
                                    skuItemOutput.Point = (float)Math.Round(rewardPointSku.Point / 1000, 2, MidpointRounding.AwayFromZero);
                                }
                            }
                            else
                            {
                                skuItemOutput.Point = (float)Math.Round(rewardPointSku.Point / 1000, 2, MidpointRounding.AwayFromZero);
                            }
                        }
                    }
                    else
                    {
                        var rewardPointSkuAll = await _redisCachingAppService.GetRewardPointSkuWithHash(skuItem.Sku, "All", skuItem.UnitCode);
                        if (rewardPointSkuAll != null)
                        {
                            skuItemOutput.Point = 0;
                            skuItemOutput.RewardPointId = rewardPointSkuAll.RewardPointId;
                            skuItemOutput.RewardPointCode = rewardPointSkuAll.RewardPointCode;

                            if (rewardPointSkuAll.Point > 0)
                            {
                                if (rewardPointSkuAll.IsQuota)
                                {
                                    var quota = await _redisCachingAppService.GetRewardQuota(rewardPointSkuAll.RewardPointId, rewardPointSkuAll.Sku);
                                    if (quota?.QuotaCurrentMinLevel > 0)
                                    {
                                        //  ===Nếu còn Quota thì trả điểm thưởng về===
                                        skuItemOutput.Point = (float)Math.Round(rewardPointSkuAll.Point / 1000, 2, MidpointRounding.AwayFromZero);
                                    }
                                }
                                else
                                {
                                    skuItemOutput.Point = (float)Math.Round(rewardPointSkuAll.Point / 1000, 2, MidpointRounding.AwayFromZero);
                                }
                            }
                        }
                    }
                    #endregion ===Get RewardPointSku===

                    if (string.IsNullOrEmpty(skuItem.Barcode))
                    {
                        result.Add(skuItemOutput);
                        continue;
                    }
                    else
                    {
                        RewardPointBarcodeMaxPointCacheItem rewardPointBarcodeMaxPointCacheItem = null;

                        #region ===Get Redis cho BarcodeMaxPoint===
                        //  ===Get Redis cho BarcodeMaxPoint===
                        try
                        {
                            rewardPointBarcodeMaxPointCacheItem = await _redisCachingAppService.GetRewardPointBarcodeMaxPointWithHash(skuItem.Barcode, skuItem.ShopCode, skuItem.UnitCode);
                        }
                        catch (Exception)
                        {
                            //  Do nothing
                        }
                        #endregion ===Get Redis cho BarcodeMaxPoint===

                        #region ===Nếu chưa có Redis cho BarcodeMaxPoint => Get BarcodeMaxPoint===
                        if (rewardPointBarcodeMaxPointCacheItem == null)
                        {
                            var productItemBarcode = await _productItemAppService.GetProductInfoByBarcode(skuItem.Barcode);
                            if (productItemBarcode != null)
                            {
                                DateTime expireDate_From = DateTime.Now.AddMonths(-1);
                                DateTime expireDate_To = DateTime.Now.AddMonths(6);
                                if (productItemBarcode.BarcodeType == 1 // 1:"Cận date (tem vàng)"
                                    && productItemBarcode.StatusCode == 1 // 1:"Active"
                                    && productItemBarcode.ExpireDate.Date >= expireDate_From.Date
                                    && productItemBarcode.ExpireDate.Date <= expireDate_To.Date
                                    )
                                {
                                    //  Barcode cận date

                                    var isExclude = false;

                                    #region ===Check Sku loại trừ cận date===
                                    //  ===Check Sku loại trừ cận date===
                                    var itemExclude = await _redisCachingAppService.GetRewardItemExcludeBarcode(productItemBarcode.ItemCode);
                                    if (itemExclude != null)
                                    {
                                        var now = DateTime.Now;
                                        if (itemExclude.Exclude_Year == now.Year && itemExclude.Exclude_Month == now.Month)
                                        {
                                            if (itemExclude.TypeCode == $"{RewardItemExcludeBarcodeTypeEnum.Sku}")
                                            {
                                                isExclude = true;
                                            }
                                            else if (itemExclude.TypeCode == $"{RewardItemExcludeBarcodeTypeEnum.ExpireDate}")
                                            {
                                                if (itemExclude.LotDate_Year == productItemBarcode.ExpireDate.Year
                                                    && itemExclude.LotDate_Month == productItemBarcode.ExpireDate.Month)
                                                {
                                                    isExclude = true;
                                                }
                                            }
                                        }
                                    }
                                    #endregion ===Check Sku loại trừ cận date===

                                    #region ===Get điểm thưởng cho barcode===
                                    if (isExclude == false)
                                    {
                                        #region ===Get RewardPointSkuBarcode===
                                        var rewardPointSkuBarcode = await _redisCachingAppService.GetRewardPointSkuBarcode(skuItem.Barcode, skuItem.ShopCode);
                                        if (rewardPointSkuBarcode != null)
                                        {
                                            float point = 0;
                                            if (skuItem.Price > 0 && rewardPointSkuBarcode.Point > 0)
                                            {
                                                point = (float)Math.Round(skuItem.Price * rewardPointSkuBarcode.Point / 100.0 / 1000, 2, MidpointRounding.AwayFromZero);
                                            }

                                            rewardPointBarcodeMaxPointCacheItem = new RewardPointBarcodeMaxPointCacheItem()
                                            {
                                                Barcode = rewardPointSkuBarcode.Barcode,
                                                ShopCode = rewardPointSkuBarcode.ShopCode,
                                                Sku = rewardPointSkuBarcode.Sku,
                                                UnitCode = skuItem.UnitCode,
                                                Point = point,
                                                ActiveFromDate = rewardPointSkuBarcode.ActiveFromDate.Date,
                                                ActiveToDate = rewardPointSkuBarcode.ActiveToDate.Date,
                                                PointTypeCode = rewardPointSkuBarcode.PointTypeCode,
                                                RewardPointId = rewardPointSkuBarcode.RewardPointId,
                                                RewardPointCode = rewardPointSkuBarcode.RewardPointCode
                                            };
                                        }
                                        #endregion ===Get RewardPointBarcode===

                                        #region ===Get RewardPointBarcode===
                                        var rewardPointBarcode = await _redisCachingAppService.GetRewardPointBarcode(skuItem.ShopCode);
                                        if (rewardPointBarcode != null)
                                        {
                                            float point = 0;
                                            if (skuItem.Price > 0 && rewardPointBarcode.Point > 0)
                                            {
                                                point = (float)Math.Round(skuItem.Price * rewardPointBarcode.Point / 100.0 / 1000, 2, MidpointRounding.AwayFromZero);
                                            }

                                            if (rewardPointBarcodeMaxPointCacheItem == null // Nếu chưa có => Set điểm mới
                                                ||
                                                (rewardPointBarcodeMaxPointCacheItem != null && point > rewardPointBarcodeMaxPointCacheItem.Point) // Nếu đã có, mà điểm mới > điểm cũ => Set điểm mới
                                                )
                                            {
                                                rewardPointBarcodeMaxPointCacheItem = new RewardPointBarcodeMaxPointCacheItem()
                                                {
                                                    Barcode = skuItem.Barcode,
                                                    ShopCode = rewardPointBarcode.ShopCode,
                                                    Sku = productItemBarcode.ItemCode,
                                                    UnitCode = skuItem.UnitCode,
                                                    Point = point,
                                                    ActiveFromDate = rewardPointBarcode.ActiveFromDate.Date,
                                                    ActiveToDate = rewardPointBarcode.ActiveToDate.Date,
                                                    PointTypeCode = rewardPointBarcode.PointTypeCode,
                                                    RewardPointId = rewardPointBarcode.RewardPointId,
                                                    RewardPointCode = rewardPointBarcode.RewardPointCode
                                                };
                                            }
                                        }
                                        #endregion ===Get RewardPointBarcode===
                                    }
                                    #endregion ===Get điểm thưởng cho barcode===
                                }

                                #region ===Cache Redis cho BarcodeMaxPoint (nếu có)===
                                //  ===Cache Redis cho BarcodeMaxPoint (nếu có)===
                                if (rewardPointBarcodeMaxPointCacheItem != null)
                                {
                                    try
                                    {
                                        var hour = 8;
                                        rewardPointBarcodeMaxPointCacheItem.CachingDateTime = DateTime.Now;
                                        rewardPointBarcodeMaxPointCacheItem.CachingExpireDateTime = DateTime.Now.AddHours(hour);
                                        var expiryTimeSpan = new TimeSpan(hour, 0, 0);
                                        await _redisCachingAppService.SetRewardPointBarcodeMaxPointWithHash(
                                            rewardPointBarcodeMaxPointCacheItem.Barcode,
                                            rewardPointBarcodeMaxPointCacheItem.ShopCode,
                                            rewardPointBarcodeMaxPointCacheItem.UnitCode,
                                            rewardPointBarcodeMaxPointCacheItem,
                                            expiryTimeSpan);
                                    }
                                    catch (Exception)
                                    {
                                        //  Do nothing
                                    }
                                }
                                else
                                {
                                    var now = DateTime.Now;
                                    rewardPointBarcodeMaxPointCacheItem = new RewardPointBarcodeMaxPointCacheItem()
                                    {
                                        Barcode = skuItem.Barcode,
                                        ShopCode = skuItem.ShopCode,
                                        Sku = skuItem.Sku,
                                        UnitCode = skuItem.UnitCode,
                                        Point = 0,
                                        ActiveFromDate = now.Date,
                                        ActiveToDate = now.Date.AddHours(1),
                                        PointTypeCode = $"{PointValueTypeEnum.Percent}",
                                        RewardPointId = Guid.Empty,
                                        RewardPointCode = ""
                                    };
                                    try
                                    {
                                        var hour = 1;
                                        rewardPointBarcodeMaxPointCacheItem.CachingDateTime = DateTime.Now;
                                        rewardPointBarcodeMaxPointCacheItem.CachingExpireDateTime = DateTime.Now.AddHours(hour);
                                        var expiryTimeSpan = new TimeSpan(hour, 0, 0);
                                        await _redisCachingAppService.SetRewardPointBarcodeMaxPointWithHash(
                                            rewardPointBarcodeMaxPointCacheItem.Barcode,
                                            rewardPointBarcodeMaxPointCacheItem.ShopCode,
                                            rewardPointBarcodeMaxPointCacheItem.UnitCode,
                                            rewardPointBarcodeMaxPointCacheItem,
                                            expiryTimeSpan);
                                    }
                                    catch (Exception)
                                    {
                                        //  Do nothing
                                    }
                                }
                                #endregion ===Cache Redis cho BarcodeMaxPoint (nếu có)===
                            }
                        }
                        #endregion ===Nếu chưa có Redis cho BarcodeMaxPoint => Get BarcodeMaxPoint===

                        #region ===Trả điểm BarcodeMaxPoint (nếu có)===
                        if (rewardPointBarcodeMaxPointCacheItem != null)
                        {

                            //  ===Trả điểm Barcode nếu > điểm Sku===
                            if (rewardPointBarcodeMaxPointCacheItem.Point > skuItemOutput.Point)
                            {
                                skuItemOutput.Point = rewardPointBarcodeMaxPointCacheItem.Point;
                                skuItemOutput.RewardPointId = rewardPointBarcodeMaxPointCacheItem.RewardPointId;
                                skuItemOutput.RewardPointCode = rewardPointBarcodeMaxPointCacheItem.RewardPointCode;
                            }
                            //  ===Trường hợp Sku không có cơ cấu nào, mà Barcode có cơ cấu nhưng 0 điểm => Trả về cơ cấu Barcode===
                            if (string.IsNullOrEmpty(skuItemOutput.RewardPointCode) && skuItemOutput.Point == 0)
                            {
                                skuItemOutput.RewardPointId = rewardPointBarcodeMaxPointCacheItem.RewardPointId;
                                skuItemOutput.RewardPointCode = rewardPointBarcodeMaxPointCacheItem.RewardPointCode;
                            }
                        }
                        #endregion ===Trả điểm BarcodeMaxPoint (nếu có)===
                    }

                    result.Add(skuItemOutput);
                }

                return result;
            }
            catch (FrtValidationException ex)
            {
                throw ex.GetBaseException();
            }
            catch (Exception ex)
            {
                throw new FrtValidationException(ErrorCodes.Error, _stringLocalizer[ErrorCodes.Error, $"{MethodBase.GetCurrentMethod().ReflectedType.FullName}: ex.Message: {ex.Message}!"]).WithData("Key", ex.Message);
            }
            finally
            {
                span?.End();
            }
        }
    }
}
