﻿using System.Collections.Generic;

namespace FRTTMO.RewardPointApplication.Dtos.ElasticSearch
{
    public class CommonDocumentResultDto<T>
    {
        public int Count { get; set; } = 0;
        public List<T> Documents { get; set; } = new List<T>();
    }
}
