﻿namespace FRTTMO.RewardPointApplication.ExceptionCodes
{
    public static class ErrorCodes
    {
        public static string Error = "FRTTMO.RewardPointApplication:00001";
        public static string Invalid = "FRTTMO.RewardPointApplication:00002";
        public static string NotExist = "FRTTMO.RewardPointApplication:00003";
        public static string ErrorElasticSearch = "FRTTMO.RewardPointApplication:00004";
    }
}
