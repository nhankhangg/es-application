﻿namespace FRTTMO.RewardPointApplication.ExceptionCodes
{
    public static class ExceptionCode
    {
        public static readonly string BadRequest = "FRTTMO.RewardPointApplication:00400";

        public static readonly string InternalServerError = "FRTTMO.RewardPointApplication:00500";

        public static readonly string BusinessException = "FRTTMO.RewardPointApplication:00403";

        public static readonly string NotFound = "FRTTMO.RewardPointApplication:00404";
    }
}
