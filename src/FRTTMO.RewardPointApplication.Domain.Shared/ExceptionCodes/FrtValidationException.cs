﻿using Microsoft.Extensions.Logging;
using System;
using System.Runtime.Serialization;
using Volo.Abp;
using Volo.Abp.ExceptionHandling;
using Volo.Abp.Logging;

namespace FRTTMO.RewardPointApplication.ExceptionCodes
{
    [Serializable]
    public class FrtValidationException : Exception, IHasErrorCode, IHasErrorDetails, IHasLogLevel, IUserFriendlyException
    {
        public string Code { get; set; }
        public string Details { get; set; }

        public LogLevel LogLevel { get; set; }

        public FrtValidationException(
            string code = null,
            string message = null,
            string details = null,
            Exception innerException = null,
            LogLevel logLevel = LogLevel.Error) : base(message, innerException)
        {

            Code = code;
            Details = details;
            LogLevel = logLevel;
        }

        public FrtValidationException(SerializationInfo serializationInfo, StreamingContext context) : base(
            serializationInfo, context)
        {

        }

        public FrtValidationException WithData(string name, object value)
        {
            Data[name] = value;
            return this;
        }
    }
}