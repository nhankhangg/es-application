﻿using Volo.Abp.Localization;

namespace FRTTMO.RewardPointApplication.Localization;

[LocalizationResourceName("RewardPointApplication")]
public class RewardPointApplicationResource
{

}
