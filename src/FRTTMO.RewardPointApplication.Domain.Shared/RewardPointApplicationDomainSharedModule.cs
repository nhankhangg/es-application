﻿using Volo.Abp.Modularity;
using Volo.Abp.Localization;
using FRTTMO.RewardPointApplication.Localization;
using Volo.Abp.Localization.ExceptionHandling;
using Volo.Abp.Validation;
using Volo.Abp.Validation.Localization;
using Volo.Abp.VirtualFileSystem;

namespace FRTTMO.RewardPointApplication;

[DependsOn(
    typeof(AbpValidationModule)
)]
public class RewardPointApplicationDomainSharedModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        Configure<AbpVirtualFileSystemOptions>(options =>
        {
            options.FileSets.AddEmbedded<RewardPointApplicationDomainSharedModule>();
        });

        Configure<AbpLocalizationOptions>(options =>
        {
            options.Resources
                .Add<RewardPointApplicationResource>("en")
                .AddBaseTypes(typeof(AbpValidationResource))
                .AddVirtualJson("/Localization/RewardPointApplication");
        });

        Configure<AbpExceptionLocalizationOptions>(options =>
        {
            options.MapCodeNamespace("RewardPointApplication", typeof(RewardPointApplicationResource));
        });
    }
}
