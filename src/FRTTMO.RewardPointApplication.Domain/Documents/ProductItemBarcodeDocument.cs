﻿using Nest;
using System;

namespace FRTTMO.RewardPointApplication.Documents
{
    [ElasticsearchType(IdProperty = nameof(Id))]
    public class ProductItemBarcodeDocument
    {
        [Text(Name = "code")]
        public string Code { get; set; }
        [Text(Name = "barcode")]
        public string Barcode { get; set; }

        [Text(Name = "itemCode")]
        public string ItemCode { get; set; }
        [Date(Name = "expireDate")]
        public DateTime? ExpireDate { get; set; }
        [Text(Name = "lotNumber")]
        public string LotNumber { get; set; }

        [Number(NumberType.Integer, Name = "typeCode")]
        public int TypeCode { get; set; }
        [Text(Name = "typeName")]
        public string TypeName { get; set; }

        [Date(Name = "createdTime")]
        public DateTime? CreatedTime { get; set; }

        [Number(NumberType.Integer, Name = "statusCode")]
        public int StatusCode { get; set; }
        [Text(Name = "statusName")]
        public string StatusName { get; set; }
    }
}
