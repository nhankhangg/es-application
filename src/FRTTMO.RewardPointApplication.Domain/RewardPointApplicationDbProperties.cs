﻿namespace FRTTMO.RewardPointApplication;

public static class RewardPointApplicationDbProperties
{
    public static string DbTablePrefix { get; set; } = "RewardPointApplication";

    public static string DbSchema { get; set; } = null;

    public const string ConnectionStringName = "RewardPointApplication";
}
