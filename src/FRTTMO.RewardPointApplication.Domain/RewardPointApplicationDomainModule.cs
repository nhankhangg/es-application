﻿using Volo.Abp.Domain;
using Volo.Abp.Modularity;

namespace FRTTMO.RewardPointApplication;

[DependsOn(
    typeof(AbpDddDomainModule),
    typeof(RewardPointApplicationDomainSharedModule)
)]
public class RewardPointApplicationDomainModule : AbpModule
{

}
