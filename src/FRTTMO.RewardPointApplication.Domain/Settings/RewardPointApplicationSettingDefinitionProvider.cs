﻿using Volo.Abp.Settings;

namespace FRTTMO.RewardPointApplication.Settings;

public class RewardPointApplicationSettingDefinitionProvider : SettingDefinitionProvider
{
    public override void Define(ISettingDefinitionContext context)
    {
        /* Define module settings here.
         * Use names from RewardPointApplicationSettings class.
         */
    }
}
