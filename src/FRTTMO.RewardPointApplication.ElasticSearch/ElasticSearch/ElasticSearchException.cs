﻿using System;
using System.Runtime.Serialization;
using Volo.Abp;

namespace FRTTMO.RewardPointApplication.ElasticSearch
{
    [Serializable]
    public class ElasticSearchException : AbpException
    {
        public ElasticSearchException()
        {

        }

        public ElasticSearchException(SerializationInfo serializationInfo, StreamingContext context) : base(serializationInfo, context)
        {

        }

        public ElasticSearchException(string message) : base(message)
        {

        }

        public ElasticSearchException(string message, Exception innerException) : base(message, innerException)
        {

        }
    }
}
