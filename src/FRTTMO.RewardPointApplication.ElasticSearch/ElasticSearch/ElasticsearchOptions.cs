﻿using System.Collections.Generic;

namespace FRTTMO.RewardPointApplication.ElasticSearch
{
    public class ElasticsearchOptions
    {
        private string _indexProductItemBarcode;

        public List<string> ConnectionStrings { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string IndexPrefix { get; set; }
        public string Environment { get; set; }

        public string IndexProductItemBarcode
        {
            get => _indexProductItemBarcode;
            set => _indexProductItemBarcode = value;
        }
    }
}
