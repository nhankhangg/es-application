﻿using Elasticsearch.Net;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Nest;
using Nest.JsonNetSerializer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FRTTMO.RewardPointApplication.ElasticSearch
{
    public class ElasticsearchService : IElasticsearchService
    {
        public IElasticClient ElasticClient { get; set; }

        protected ElasticsearchOptions ElasticsearchOptions { get; }

        private readonly ILogger<ElasticsearchService> _logger;

        public ElasticsearchService(IOptions<ElasticsearchOptions> options, ILogger<ElasticsearchService> logger)
        {
            ElasticsearchOptions = options.Value;
            ElasticClient = GetClient();
            _logger = logger;
        }

        private ElasticClient GetClient()
        {
            if (ElasticsearchOptions.ConnectionStrings.Count == 0)
                throw new ElasticSearchException("Elasticsearch ConnectionString Incorrect");

            ConnectionSettings connectionSettings;

            if (ElasticsearchOptions.ConnectionStrings.Count > 1)
            {
                var nodes = ElasticsearchOptions.ConnectionStrings.Select(s => new Uri(s)).ToList();
                var connectionPool = new StaticConnectionPool(nodes);
                connectionSettings = new ConnectionSettings(connectionPool, sourceSerializer: JsonNetSerializer.Default);
            }
            else
            {
                var node = ElasticsearchOptions.ConnectionStrings.Select(s => new Uri(s)).FirstOrDefault();

                connectionSettings = new ConnectionSettings(node);
            }

            connectionSettings.EnableDebugMode()
                              .PrettyJson()
                              .RequestTimeout(TimeSpan.FromMinutes(2));
            if (!String.IsNullOrEmpty(ElasticsearchOptions.UserName) && !String.IsNullOrEmpty(ElasticsearchOptions.Password))
            {
                connectionSettings.ServerCertificateValidationCallback((o, certificate, chain, errors) => true);
                connectionSettings.ServerCertificateValidationCallback(CertificateValidations.AllowAll);
                connectionSettings.BasicAuthentication(ElasticsearchOptions.UserName, ElasticsearchOptions.Password);
            }
            var client = new ElasticClient(connectionSettings);

            return client;
        }

        public virtual async Task CreateIndexAsync(string indexName, int shard = 1, int numberOfReplicas = 1)
        {
            var exits = await ElasticClient.Indices.ExistsAsync(indexName);

            if (exits.Exists)
                return;
            var result = await ElasticClient
                .Indices.CreateAsync(indexName,
                    ss =>
                        ss.Index(indexName)
                            .Settings(
                                o => o.NumberOfShards(shard).NumberOfReplicas(numberOfReplicas)
                                    .Setting("max_result_window", int.MaxValue)));
            if (result.Acknowledged)
            {
                return;
            }
            throw new ElasticSearchException($"Create Index {indexName} failed :" + result.ServerError.Error.Reason);
        }

        public virtual async Task CreateIndexAsync<T, TKey>(string indexName, int shard = 1, int numberOfReplicas = 1) where T : class
        {
            var exits = await ElasticClient.Indices.ExistsAsync(indexName);

            if (exits.Exists)
                return;
            var result = await ElasticClient
                .Indices.CreateAsync(indexName,
                    ss =>
                        ss.Index(indexName)
                            .Settings(
                                o => o.NumberOfShards(shard).NumberOfReplicas(numberOfReplicas)
                                    .Setting("max_result_window", int.MaxValue))
                            .Map(m => m.AutoMap<T>()));
            if (result.Acknowledged)
            {
                return;
            }

            throw new ElasticSearchException($"Create Index {indexName} failed : :" + result.ServerError.Error.Reason);
        }

        public virtual async Task ReIndex<T, TKey>(string indexName) where T : class
        {
            await DeleteIndexAsync(indexName);
            await CreateIndexAsync<T, TKey>(indexName);
        }
        public async Task<T> InsertAsync<T,Tkey>(string indexName, T model) where T : class
        {
            var createResponse = await ElasticClient.IndexAsync(new IndexRequest<T>(model, indexName));

            if (createResponse.ServerError != null)
            {
                var status = createResponse.ServerError.Status;
                var message = createResponse.ServerError.Error.ToString();
                throw new ElasticSearchException($"Bulk InsertOrUpdate Docuemnt failed at index {indexName} :{status}-{message}");
            }

            return model;
        }
        public async Task<bool> UpdateAsync<T, Tkey>(string indexName, string id, T model) where T : class
        {
            var updateResponse = await ElasticClient.UpdateAsync<T>(id, u => u.Index(indexName).Doc(model));

            if (updateResponse.ServerError != null)
            {
                var status = updateResponse.ServerError.Status;
                var message = updateResponse.ServerError.Error.ToString();
                throw new ElasticSearchException($"Bulk InsertOrUpdate Docuemnt failed at index {indexName} :{status}-{message}");
            }

            return updateResponse.IsValid;
        }

        public virtual async Task AddOrUpdateAsync<T, TKey>(string indexName, T model) where T : class
        {
            var exits = ElasticClient.DocumentExists<T>(DocumentPath<T>.Id(new Id(model)), dd => dd.Index(indexName));

            if (exits.Exists)
            {
                var result = await ElasticClient.UpdateAsync(DocumentPath<T>.Id(new Id(model)),
                    ss => ss.Index(indexName).Doc(model).RetryOnConflict(3));

                if (result.ServerError == null) return;
                throw new ElasticSearchException($"Update Document failed at index{indexName} :" +
                                                 result.ServerError.Error.Reason);
            }
            else
            {
                var result = await ElasticClient.IndexAsync<T>(model, ss => ss.Index(indexName));
                if (result.ServerError == null) return;
                throw new ElasticSearchException($"Insert Docuemnt failed at index {indexName} :" +
                                                 result.ServerError.Error.Reason);
            }
        }

        public virtual async Task BulkAddorUpdateAsync<T, TKey>(string indexName, List<T> list, int bulkNum = 100000) where T : class
        {
            await BulkAddOrUpdate<T, TKey>(indexName, list);
        }
        private async Task BulkAddOrUpdate<T, TKey>(string indexName, List<T> list) where T : class
        {
            var bulk = new BulkRequest(indexName)
            {
                Operations = new List<IBulkOperation>()
            };
            foreach (var item in list)
            {
                bulk.Operations.Add(new BulkIndexOperation<T>(item));
            }

            var response = await ElasticClient.BulkAsync(bulk);
            if (response.Errors)
                throw new ElasticSearchException(
                    $"Bulk InsertOrUpdate Docuemnt failed at index {indexName} :{response.ServerError.Error.Reason}");
        }
        private async Task BulkDelete<T, TKey>(string indexName, List<T> list) where T : class
        {
            var bulk = new BulkRequest(indexName)
            {
                Operations = new List<IBulkOperation>()
            };
            foreach (var item in list)
            {
                bulk.Operations.Add(new BulkDeleteOperation<T>(new Id(item)));
            }

            var response = await ElasticClient.BulkAsync(bulk);
            if (response.Errors)
                throw new ElasticSearchException(
                    $"Bulk Delete Docuemnt at index {indexName} :{response.ServerError.Error.Reason}");
        }
        public virtual async Task BulkDeleteAsync<T, TKey>(string indexName, List<T> list, int bulkNum = 10000) where T : class
        {
            if (list.Count <= bulkNum)
                await BulkDelete<T, TKey>(indexName, list);
            else
            {
                var total = (int)Math.Ceiling(list.Count * 1.0f / bulkNum);
                var tasks = new List<Task>();
                for (var i = 0; i < total; i++)
                {
                    var i1 = i;
                    tasks.Add(Task.Factory.StartNew(() =>
                        BulkDelete<T, TKey>(indexName, list.Skip(i1 * bulkNum).Take(bulkNum).ToList())));
                }

                await Task.WhenAll(tasks);
            }
        }

        public virtual async Task DeleteAsync<T, TKey>(string indexName, T model) where T : class
        {
            var response = await ElasticClient.DeleteAsync(new DeleteRequest(indexName, new Id(model)));
            if (response.ServerError == null) return;
            throw new Exception($"Delete Docuemnt at index {indexName} :{response.ServerError.Error.Reason}");
        }

        public virtual async Task DeleteIndexAsync(string indexName)
        {
            var response = await ElasticClient.Indices.DeleteAsync(indexName);
            if (response.Acknowledged) return;
            throw new Exception($"Delete index {indexName} failed :{response.ServerError.Error.Reason}");
        }


        public virtual async Task<ISearchResponse<T>> SearchAsync<T, TKey>(string indexName, SearchDescriptor<T> query, int skip, int size, string[] includeFields = null, string preTags = "<strong style=\"color: red;\">", string postTags = "</strong>", bool disableHigh = false, params string[] highField) where T : class
        {
            query.Index(indexName);
            var highlight = new HighlightDescriptor<T>();
            if (disableHigh)
            {
                preTags = "";
                postTags = "";
            }

            highlight.PreTags(preTags).PostTags(postTags);

            var isHigh = highField != null && highField.Length > 0;

            var hfs = new List<Func<HighlightFieldDescriptor<T>, IHighlightField>>();


            query.Skip(skip).Take(size);

            if (isHigh)
            {
                foreach (var s in highField)
                {
                    hfs.Add(f => f.Field(s));
                }
            }

            highlight.Fields(hfs.ToArray());
            query.Highlight(h => highlight);
            if (includeFields != null)
                query.Source(ss => ss.Includes(ff => ff.Fields(includeFields.ToArray())));
            string json = ElasticClient.RequestResponseSerializer.SerializeToString(query);
            _logger.LogInformation("Request sent ELS: {0}", json);
            var response = await ElasticClient.SearchAsync<T>(query);
            return response;
        }

        public async Task<ISearchResponse<T>> SearchAsync<T, TDto>(ISearchRequest query, int skip, int size) where T : class
        {
            query.From = skip;
            query.Size = size;
            var response = await ElasticClient.SearchAsync<T>(query);
            return response;
        }

        public async Task<ISearchResponse<T>> SearchAsync<T, TDto>(ISearchRequest query) where T : class
        {
            var response = await ElasticClient.SearchAsync<T>(query);
            return response;
        }

        public virtual async Task<CountResponse> CountAsync<T, TKey>(string indexName, Func<QueryContainerDescriptor<T>, QueryContainer> query) where T : class
        {
            var response = await ElasticClient.CountAsync<T>(c => c.Index(indexName).Query(query));

            return response;
        }
    }
}
