﻿using Nest;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
namespace FRTTMO.RewardPointApplication.ElasticSearch
{
    public interface IElasticsearchService : ITransientDependency
    {
        Task CreateIndexAsync(string indexName, int shard = 1, int numberOfReplicas = 1);
        Task CreateIndexAsync<T, TKey>(string indexName, int shard = 1, int numberOfReplicas = 1)
            where T : class;
        Task ReIndex<T, TKey>(string indexName) where T : class;
        Task<bool> UpdateAsync<T, Tkey>(string indexName, string id, T model) where T : class;
        Task<T> InsertAsync<T, Tkey>(string indexName, T model) where T : class;
        Task AddOrUpdateAsync<T, TKey>(string indexName, T model) where T : class;
        Task BulkAddorUpdateAsync<T, TKey>(string indexName, List<T> list, int bulkNum = 1000)
            where T : class;
        Task BulkDeleteAsync<T, TKey>(string indexName, List<T> list, int bulkNum = 1000) where T : class;
        Task DeleteAsync<T, TKey>(string indexName, T model) where T : class;
        Task DeleteIndexAsync(string indexName);
        Task<ISearchResponse<T>> SearchAsync<T, TKey>(string indexName, SearchDescriptor<T> query,
            int skip, int size, string[] includeFields = null, string preTags = "<strong style=\"color: red;\">",
            string postTags = "</strong>", bool disableHigh = false, params string[] highField)
            where T : class;
        Task<ISearchResponse<T>> SearchAsync<T, TDto>(ISearchRequest query, int skip, int size) where T : class;
        Task<ISearchResponse<T>> SearchAsync<T, TDto>(ISearchRequest query) where T : class;
        Task<CountResponse> CountAsync<T, TKey>(string indexName,
           Func<QueryContainerDescriptor<T>, QueryContainer> query) where T : class;
    }
}
