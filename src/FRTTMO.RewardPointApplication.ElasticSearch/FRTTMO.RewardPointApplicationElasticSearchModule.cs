﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Modularity;

namespace FRTTMO.RewardPointApplication.ElasticSearch
{
    public class RewardPointApplicationElasticSearchModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            var configuration = context.Services.GetConfiguration();
            Configure<ElasticsearchOptions>(configuration.GetSection("ElasticSearch"));
            context.Services.AddSingleton<IElasticsearchService, ElasticsearchService>();
        }
    }
}
