﻿using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;

namespace FRTTMO.RewardPointApplication.EntityFrameworkCore;

[ConnectionStringName(RewardPointApplicationDbProperties.ConnectionStringName)]
public interface IRewardPointApplicationDbContext : IEfCoreDbContext
{
    /* Add DbSet for each Aggregate Root here. Example:
     * DbSet<Question> Questions { get; }
     */
}
