﻿using Microsoft.EntityFrameworkCore;
using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;

namespace FRTTMO.RewardPointApplication.EntityFrameworkCore;

[ConnectionStringName(RewardPointApplicationDbProperties.ConnectionStringName)]
public class RewardPointApplicationDbContext : AbpDbContext<RewardPointApplicationDbContext>, IRewardPointApplicationDbContext
{
    /* Add DbSet for each Aggregate Root here. Example:
     * public DbSet<Question> Questions { get; set; }
     */

    public RewardPointApplicationDbContext(DbContextOptions<RewardPointApplicationDbContext> options)
        : base(options)
    {

    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);

        builder.ConfigureRewardPointApplication();
    }
}
