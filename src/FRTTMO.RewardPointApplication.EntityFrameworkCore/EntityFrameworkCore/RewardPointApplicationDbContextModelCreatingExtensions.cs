﻿using Microsoft.EntityFrameworkCore;
using Volo.Abp;

namespace FRTTMO.RewardPointApplication.EntityFrameworkCore;

public static class RewardPointApplicationDbContextModelCreatingExtensions
{
    public static void ConfigureRewardPointApplication(
        this ModelBuilder builder)
    {
        Check.NotNull(builder, nameof(builder));

        /* Configure all entities here. Example:

        builder.Entity<Question>(b =>
        {
            //Configure table & schema name
            b.ToTable(RewardPointApplicationDbProperties.DbTablePrefix + "Questions", RewardPointApplicationDbProperties.DbSchema);

            b.ConfigureByConvention();

            //Properties
            b.Property(q => q.Title).IsRequired().HasMaxLength(QuestionConsts.MaxTitleLength);

            //Relations
            b.HasMany(question => question.Tags).WithOne().HasForeignKey(qt => qt.QuestionId);

            //Indexes
            b.HasIndex(q => q.CreationTime);
        });
        */
    }
}
