﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace FRTTMO.RewardPointApplication.EntityFrameworkCore;

[DependsOn(
    typeof(RewardPointApplicationDomainModule),
    typeof(AbpEntityFrameworkCoreModule)
)]
public class RewardPointApplicationEntityFrameworkCoreModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        context.Services.AddAbpDbContext<RewardPointApplicationDbContext>(options =>
        {
                /* Add custom repositories here. Example:
                 * options.AddRepository<Question, EfCoreQuestionRepository>();
                 */
        });
    }
}
