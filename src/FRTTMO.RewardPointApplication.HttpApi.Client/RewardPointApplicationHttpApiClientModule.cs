﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Http.Client;
using Volo.Abp.Modularity;
using Volo.Abp.VirtualFileSystem;

namespace FRTTMO.RewardPointApplication;

[DependsOn(
    typeof(RewardPointApplicationApplicationContractsModule),
    typeof(AbpHttpClientModule))]
public class RewardPointApplicationHttpApiClientModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        context.Services.AddHttpClientProxies(
            typeof(RewardPointApplicationApplicationContractsModule).Assembly,
            RewardPointApplicationRemoteServiceConsts.RemoteServiceName
        );

        Configure<AbpVirtualFileSystemOptions>(options =>
        {
            options.FileSets.AddEmbedded<RewardPointApplicationHttpApiClientModule>();
        });

    }
}
