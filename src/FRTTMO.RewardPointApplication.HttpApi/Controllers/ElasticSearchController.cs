﻿using FRTTMO.RewardPointApplication.Documents;
using FRTTMO.RewardPointApplication.Dtos.ElasticSearch;
using FRTTMO.RewardPointApplication.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Volo.Abp;

namespace FRTTMO.RewardPointApplication.Controllers
{
    [Area(RewardPointApplicationRemoteServiceConsts.ModuleName)]
    [RemoteService(Name = RewardPointApplicationRemoteServiceConsts.RemoteServiceName)]
    [Route("api/elastic-search")]
    public class ElasticSearchController : RewardPointApplicationController, IElasticSearchAppService
    {
        private readonly IElasticSearchAppService _elasticSearchAppService;

        public ElasticSearchController(IElasticSearchAppService elasticSearchAppService)
        {
            _elasticSearchAppService = elasticSearchAppService;
        }

        ////////[HttpGet]
        ////////[Route("get-long-chau-cms-shop-active")]
        ////////public async Task<CommonDocumentResultDto<LongChauCMSShopDocument>> GetLongChauCMSShopActive(int skip = 0, int size = 10)
        ////////{
        ////////    return await _elasticSearchAppService.GetLongChauCMSShopActive(skip, size);
        ////////}

        ////////[HttpGet]
        ////////[Route("get-reward-point-sales-approved")]
        ////////public async Task<CommonDocumentResultDto<RewardPointSalesDocument>> GetRewardPointSalesApproved(DateTime effectiveDate, int skip = 0, int size = 10)
        ////////{
        ////////    return await _elasticSearchAppService.GetRewardPointSalesApproved(effectiveDate, skip, size);
        ////////}

        ////////[HttpGet]
        ////////[Route("get-reward-point-sales-by-id")]
        ////////public async Task<RewardPointSalesDocument> GetRewardPointSalesById(string id) => await _elasticSearchAppService.GetRewardPointSalesById(id);

        ////////[HttpGet]
        ////////[Route("get-reward-point-sales-details")]
        ////////public async Task<CommonDocumentResultDto<RewardPointSalesDetailsDocument>> GetRewardPointSalesDetails(string rewardPointId, int skip = 0, int size = 10)
        ////////{
        ////////    return await _elasticSearchAppService.GetRewardPointSalesDetails(rewardPointId, skip, size);
        ////////}

        ////////[HttpGet]
        ////////[Route("get-product-item-barcode-before-date-active")]
        ////////public async Task<CommonDocumentResultDto<ProductItemBarcodeDocument>> GetProductItemBarcodeBeforeDateActive(int skip = 0, int size = 10)
        ////////{
        ////////    return await _elasticSearchAppService.GetProductItemBarcodeBeforeDateActive(skip, size);
        ////////}

        [HttpGet]
        [Route("product-item/barcode/{barcode}")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<CommonDocumentResultDto<ProductItemBarcodeDocument>> ProductItem_Barcode(string barcode, int skip = 0, int size = 10)
        {
            return await _elasticSearchAppService.ProductItem_Barcode(barcode, skip, size);
        }

    }
}
