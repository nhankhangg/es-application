﻿using FRTTMO.RewardPointApplication.Documents;
using FRTTMO.RewardPointApplication.Dtos.ProductItem;
using FRTTMO.RewardPointApplication.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Volo.Abp;

namespace FRTTMO.RewardPointApplication.Controllers
{
    [Area(RewardPointApplicationRemoteServiceConsts.ModuleName)]
    [RemoteService(Name = RewardPointApplicationRemoteServiceConsts.RemoteServiceName)]
    [Route("api/product-item")]
    public class ProductItemController : RewardPointApplicationController, IProductItemAppService
    {
        private readonly IProductItemAppService _productItemAppService;

        public ProductItemController(IProductItemAppService productItemAppService)
        {
            _productItemAppService = productItemAppService;
        }

        [HttpGet]
        [Route("get-product-info-by-barcode/{barcode}")]
        public async Task<ProductInfoByBarcodeDto> GetProductInfoByBarcode(string barcode)
        {
            return await _productItemAppService.GetProductInfoByBarcode(barcode);
        }

    }
}
