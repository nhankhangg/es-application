﻿using FRTTMO.RewardPointApplication.CacheItems;
using FRTTMO.RewardPointApplication.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Volo.Abp;

namespace FRTTMO.RewardPointApplication.Controllers
{
    [Area(RewardPointApplicationRemoteServiceConsts.ModuleName)]
    [RemoteService(Name = RewardPointApplicationRemoteServiceConsts.RemoteServiceName)]
    [Route("api/redis-caching")]
    public class RedisCachingController : RewardPointApplicationController
    {
        private readonly IRedisCachingAppService _redisCachingAppService;

        public RedisCachingController(IRedisCachingAppService redisCachingAppService)
        {
            _redisCachingAppService = redisCachingAppService;
        }

        /// <summary>Lấy ProductItemBarcode từ Redis</summary>
        [HttpGet]
        [Route("get-product-item-barcode/{barcode}")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<ProductItemBarcodeCacheItem> GetProductItemBarcode(string barcode) => await _redisCachingAppService.GetProductItemBarcode(barcode);

        /// <summary>Xóa All RewardPointBarcodeMaxPoint từ Redis</summary>
        [HttpDelete]
        [Route("remove-all-reward-point-barcode-max-point-cache")]
        public async Task<bool> RemoveAllRewardPointBarcodeMaxPointCache() => await _redisCachingAppService.RemoveAllRewardPointBarcodeMaxPointCache();
    }
}
