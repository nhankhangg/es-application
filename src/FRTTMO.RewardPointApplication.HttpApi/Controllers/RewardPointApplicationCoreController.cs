﻿using FRTTMO.RewardPointApplication.Dtos;
using FRTTMO.RewardPointApplication.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp;

namespace FRTTMO.RewardPointApplication.Controllers
{
    [Area(RewardPointApplicationRemoteServiceConsts.ModuleName)]
    [RemoteService(Name = RewardPointApplicationRemoteServiceConsts.RemoteServiceName)]
    [Route("api/reward-point-application")]
    public class RewardPointApplicationCoreController : RewardPointApplicationController, IRewardPointApplicationAppService
    {
        private readonly IRewardPointApplicationAppService _rewardPointApplicationAppService;

        public RewardPointApplicationCoreController(IRewardPointApplicationAppService rewardPointApplicationAppService)
        {
            _rewardPointApplicationAppService = rewardPointApplicationAppService;
        }

        /// <summary>Lấy code</summary>
        [HttpGet]
        [Route("get-code")]
        public string GetCode() => _rewardPointApplicationAppService.GetCode();

        /// <summary>Get RewardPoints</summary>
        [HttpGet]
        [Route("get-reward-points")] // NOTE: Không đổi
        public async Task<List<RewardPointsOutputDto>> GetRewardPoints(RewardPointsInputDto input)
        {
            return await _rewardPointApplicationAppService.GetRewardPoints(input);
        }

        /// <summary>Get RewardPoints By Skus</summary>
        [HttpPost]
        [Route("get-reward-points-by-skus")] // NOTE: Không đổi
        public async Task<List<RewardPointsBySkusOutputDto>> GetRewardPointsBySkus(List<RewardPointsBySkusInputDto> input)
        {
            return await _rewardPointApplicationAppService.GetRewardPointsBySkus(input);
        }
    }
}
