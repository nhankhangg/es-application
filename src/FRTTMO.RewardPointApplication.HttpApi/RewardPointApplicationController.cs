﻿using FRTTMO.RewardPointApplication.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace FRTTMO.RewardPointApplication;

public abstract class RewardPointApplicationController : AbpControllerBase
{
    protected RewardPointApplicationController()
    {
        LocalizationResource = typeof(RewardPointApplicationResource);
    }
}
