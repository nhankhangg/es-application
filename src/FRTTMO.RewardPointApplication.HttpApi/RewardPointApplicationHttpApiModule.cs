﻿using FRTTMO.RewardPointApplication.Localization;
using System.Net;
using FRTTMO.RewardPointApplication.ExceptionCodes;
using Localization.Resources.AbpUi;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.AspNetCore.ExceptionHandling;

namespace FRTTMO.RewardPointApplication;

[DependsOn(
    typeof(RewardPointApplicationApplicationContractsModule),
    typeof(AbpAspNetCoreMvcModule))]
public class RewardPointApplicationHttpApiModule : AbpModule
{
    public override void PreConfigureServices(ServiceConfigurationContext context)
    {
        PreConfigure<IMvcBuilder>(mvcBuilder =>
        {
            mvcBuilder.AddApplicationPartIfNotExists(typeof(RewardPointApplicationHttpApiModule).Assembly);
        });
    }

    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        Configure<AbpLocalizationOptions>(options =>
        {
            options.Resources
                .Get<RewardPointApplicationResource>()
                .AddBaseTypes(typeof(AbpUiResource));
        });
        
        Configure<AbpExceptionHttpStatusCodeOptions>(options =>
        {
            options.Map(ExceptionCode.NotFound, HttpStatusCode.NotFound);
            options.Map(ExceptionCode.BadRequest, HttpStatusCode.BadRequest);
            options.Map(ExceptionCode.InternalServerError, HttpStatusCode.InternalServerError);
        });
    }
}
