﻿using Volo.Abp.Data;
using Volo.Abp.MongoDB;

namespace FRTTMO.RewardPointApplication.MongoDB;

[ConnectionStringName(RewardPointApplicationDbProperties.ConnectionStringName)]
public interface IRewardPointApplicationMongoDbContext : IAbpMongoDbContext
{
    /* Define mongo collections here. Example:
     * IMongoCollection<Question> Questions { get; }
     */
}
