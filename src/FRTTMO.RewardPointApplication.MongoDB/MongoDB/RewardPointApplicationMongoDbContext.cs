﻿using Volo.Abp.Data;
using Volo.Abp.MongoDB;

namespace FRTTMO.RewardPointApplication.MongoDB;

[ConnectionStringName(RewardPointApplicationDbProperties.ConnectionStringName)]
public class RewardPointApplicationMongoDbContext : AbpMongoDbContext, IRewardPointApplicationMongoDbContext
{
    /* Add mongo collections here. Example:
     * public IMongoCollection<Question> Questions => Collection<Question>();
     */

    protected override void CreateModel(IMongoModelBuilder modelBuilder)
    {
        base.CreateModel(modelBuilder);

        modelBuilder.ConfigureRewardPointApplication();
    }
}
