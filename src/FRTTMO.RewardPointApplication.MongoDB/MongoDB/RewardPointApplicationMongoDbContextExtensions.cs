﻿using Volo.Abp;
using Volo.Abp.MongoDB;

namespace FRTTMO.RewardPointApplication.MongoDB;

public static class RewardPointApplicationMongoDbContextExtensions
{
    public static void ConfigureRewardPointApplication(
        this IMongoModelBuilder builder)
    {
        Check.NotNull(builder, nameof(builder));
    }
}
