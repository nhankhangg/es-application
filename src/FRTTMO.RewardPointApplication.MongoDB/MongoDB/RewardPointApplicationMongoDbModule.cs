﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Modularity;
using Volo.Abp.MongoDB;

namespace FRTTMO.RewardPointApplication.MongoDB;

[DependsOn(
    typeof(RewardPointApplicationDomainModule),
    typeof(AbpMongoDbModule)
    )]
public class RewardPointApplicationMongoDbModule : AbpModule
{
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
        context.Services.AddMongoDbContext<RewardPointApplicationMongoDbContext>(options =>
        {
                /* Add custom repositories here. Example:
                 * options.AddRepository<Question, MongoQuestionRepository>();
                 */
        });
    }
}
